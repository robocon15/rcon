//all the pins are defined here
#ifndef _BOT_CONFIG_
#define _BOT_CONFIG_
/* ********** */
#define PWM_PIN  11
#define DIR1_PIN 34
#define DIR2_PIN 36
#define COUNT_CONST 3    //count contant for smooth function

//#define PWM_PINR  11
//#define DIR1_PINR 36
//#define DIR2_PINR 34
//#define COUNT_CONST 3    //count contant for smooth function
/* ************ */

/* Double throw piston hand*/
#define DPNEUM_PIN_1 -1
#define DPNEUM_PIN_2 -1

/* *********** */

#define PNEU1_PIN1 1
#define PNEU1_PIN2 2

#define PNEU2_PIN1 3
#define PNEU2_PIN2 4

#define PNEU3_PIN1 5
#define PNEU3_PIN2 6

#define PNEU4_PIN1 7
#define PNEU4_PIN2 8

/* *********** */

/*Push pneumatic*/
#define P1_PIN1 A5		//changed
#define P1_PIN2 A4		//changed

/*SWING see-saw pneumatic*/
#define P2_PIN1 52
#define P2_PIN2 54

/* gripper pneumatic*/
#define P3_PIN1 A10
#define P3_PIN2 A11

/*up-down pneumatic*/
#define P4_PIN1 48
#define P4_PIN2 50

/*gripper 90 degree*/
#define P5_PIN1 A12
#define P5_PIN2 A13

/* *********** */

/* Single throw pneumatic*/
#define PNEUM_PIN -1

/* ********** */

/* Numpad pins   */
//#define PIN_NUM_1 1
//#define PIN_NUM_2 2
//#define PIN_NUM_3 3
//#define PIN_NUM_4 4
//#define PIN_NUM_5 5
//#define PIN_NUM_6 6
//#define PIN_NUM_7 7
//#define PIN_NUM_8 36
//#define PIN_NUM_9 38
/* ********** */


#define BUMPER_PIN 4
/* ********** */
//encoder pin
#define CHANNEL1_PIN 2           //in atachinterrupt 2 corresponds to 0
#define CHANNEL2_PIN  3			//in atachinterrupt 3 corresponds to 1
#define PPM 464
/* ********** */


//wireless
//for back motor
// #define PWM_PINB  6
// #define DIR1_PINB 38
// #define DIR2_PINB 40

// //for front motor
// #define PWM_PINF  8
// #define DIR1_PINF 24
// #define DIR2_PINF 22

// //for right motor						//direction pin reversed
// #define PWM_PINR  7
// #define DIR1_PINR 42
// #define DIR2_PINR 44

// //for left motor
// #define PWM_PINL 3
// #define DIR1_PINL 32
// #define DIR2_PINL 30



//Wired

//mechtex bot

// //for left motor 2
// #define PWM_PINL  12
// #define DIR1_PINL 40
// #define DIR2_PINL 38

// //for right motor 4
// #define PWM_PINR  8
// #define DIR1_PINR 22
// #define DIR2_PINR 24

// //for back motor3					//direction pin reversed
// #define PWM_PINB  10
// #define DIR1_PINB 32
// #define DIR2_PINB 30

// //for front motor 1
// #define PWM_PINF 11
// #define DIR1_PINF 36
// #define DIR2_PINF 34

/* ********** */

//Maxon bot

#define PWM_PINF  12
#define DIR1_PINF 38
#define DIR2_PINF 40

//for right motor 4
#define PWM_PINB  8
#define DIR1_PINB 24
#define DIR2_PINB 22

//for back motor3					//direction pin reversed
#define PWM_PINL  10
#define DIR1_PINL 30
#define DIR2_PINL 32

//for front motor 1
#define PWM_PINR 11
#define DIR1_PINR 36
#define DIR2_PINR 34


//Mecanum
//for back left motor 2
#define PWM_PINBL  12
#define DIR1_PINBL 40
#define DIR2_PINBL 38

//for front left motor 4
#define PWM_PINFL  8
#define DIR1_PINFL 22
#define DIR2_PINFL 24

//for back right motor 3
#define PWM_PINBR  10
#define DIR1_PINBR 32
#define DIR2_PINBR 30

//for front right motor 1
#define PWM_PINFR 11
#define DIR1_PINFR 34
#define DIR2_PINFR 36

/* ********** */


//for Service-motor
#define PWM_PIN1  9
#define DIR1_PIN1 28  //48
#define DIR2_PIN1 26  //46
//for Service Encoder
#define ENC_PIN1 18
#define ENC_PIN2 19


//for Vertical slider in manual..motor2
#define PWM_PIN2  9
#define DIR1_PIN2 26
#define DIR2_PIN2 28

/*//for rotating belt motor..motor3
#define PWM_PIN3  45
#define DIR1_PIN3 41
#define DIR2_PIN3 43*/

//for rotation motor of gripper
#define PWM_PIN4  7
#define DIR1_PIN4 23
#define DIR2_PIN4 25

/* ********** */

//ps2 controller
#define CLOCK  A3
#define COMMAND A1
#define ATTENTION A2
#define DATA A0
/* ********** */

// bot specifications
//everything in SI units
#define WHEEL_DISTANCE 0.2
#define A_MAX  10
#define V_MAX 10


//proximity
#define PROXIN 7


//Linear Encoder
#define LENCODER1 6        // vertical slider
#define LENCODER2 2			//for rack
#endif
