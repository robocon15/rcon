#include <unistd.h>
#include <bits/stdc++.h>

#define ARD "/dev/ttyACM0"

using namespace std;

int main()
{
	FILE *file;
	while(1)
	{
		int Buttons[] = {0,1,1,0,0,1,1};
		char n = 0;
		for(int i = 0; i<7; i++)
		{
			if(Buttons[i])
				n += pow(2,i);
		}

		printf("%d\n", n);

		n = 0;
		file = fopen(ARD, "w");

		fprintf(file, "%c",152);
		fprintf(file, "%c",152);
		fprintf(file, "%c",127);
		fprintf(file, "%c",127);
		fprintf(file, "%c", n);
		fclose(file);
		usleep(200000);
	}
}

