#ifndef ENCODER_H
#define ENCODER_H
#define PPR 1000

//Written by Rahul Gupta and Prince Kumar on 3/11/14

class Encoder
{

public:
	int diff, prev;
	double vel;
	double rpm;
	double time;
	double prevtime;
	double angle;
	int direction;
	int encoderPinA;
	int encoderPinB;
	int encoderPos;
	int encoderPinALast;
	int n;


public:
	Encoder();
	Encoder(int PinA, int PinB);
	void attachPins(int PinA, int PinB);
	void Interrupt();
	void InterruptA();
	void InterruptB();
	int getDirection();
	int getEncoderPos();
	double getRPM();
	double getAngle();
	double getAngularVel();
};

#endif
