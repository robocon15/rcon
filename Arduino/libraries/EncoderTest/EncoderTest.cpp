#include <Arduino.h>
#include "EncoderTest.h"
//#include "botconfig.h"


Encoder::Encoder()
{
	attachPins(-1, -1);
}


Encoder::Encoder(int PinA, int PinB)
{
	attachPins(PinA, PinB);
}


void Encoder::attachPins(int PinA, int PinB)
{
	encoderPinA = PinA;
	encoderPinB = PinB;
	prevtime = 0;
	diff = 0;
	prev = 0;
	vel=0;
	rpm=0;
	time=0;
	angle=0;
	encoderPos=0;
	encoderPinALast=0;
	n=0;
	pinMode(encoderPinA, INPUT);
	pinMode(encoderPinB, INPUT);
}

void Encoder::Interrupt()
{
	n = digitalRead(encoderPinA);
	if ((encoderPinALast == 0) && (n == 1))
	{
		if (digitalRead(encoderPinB) == 0)
		{
			encoderPos--;
			direction = -1;
		}
		else
		{
			encoderPos++;
			direction = 1;
		}
	}

	encoderPinALast = n;

	time = millis() - prevtime;

	if (time >= 10)
	{
		diff = encoderPos - prev;
		vel = (2 * 3141.59*diff) / (PPR*time);
		rpm = vel*9.55;
		angle = (double)encoderPos*360/PPR;
		prevtime = prevtime + time;
		prev = encoderPos;
	}
}

void Encoder::InterruptA()
{
	// look for a low-to-high on channel A
	if (digitalRead(encoderPinA) == 1) {
		// check channel B to see which way encoder is turning
		if (digitalRead(encoderPinB) == 0) {
			encoderPos++;         // CW
			direction = 1;
		}
		else {
			encoderPos--;         // CCW
			direction = -1;
		}
	}
	else   // must be a high-to-low edge on channel A                                       
	{
		// check channel B to see which way encoder is turning  
		if (digitalRead(encoderPinB) == 1) {
			encoderPos++;          // CW
			direction = 1;
		}
		else {
			encoderPos--;
			direction = -1;// CCW
		}
	}
	// Serial.println (encoderPos, DEC);          
	// use for debugging - remember to comment out
	time = millis() - prevtime;

	if (time >= 10)
	{
		diff = encoderPos - prev;
		vel = (2 * 3141.59*diff) / (PPR*time);
		rpm = vel*9.55;
		angle = (double)encoderPos*90/PPR;
		prevtime = prevtime + time;
		prev = encoderPos;
	}
}


void Encoder::InterruptB()
{
	// look for a low-to-high on channel B
	if (digitalRead(encoderPinB) == 1) {
		// check channel A to see which way encoder is turning
		if (digitalRead(encoderPinA) == 1) {
			encoderPos++;
			direction = 1;      // CW
		}
		else {
			encoderPos--;
			direction = -1;         // CCW
		}
	}
	// Look for a high-to-low on channel B
	else {
		// check channel B to see which way encoder is turning  
		if (digitalRead(encoderPinA) == 0) {
			encoderPos = encoderPos + 1;
			direction = 1;         // CW
		}
		else {
			encoderPos = encoderPos - 1;
			direction = -1;     // CCW
		}
	}

	time = millis() - prevtime;

	if (time >= 10)
	{
		diff = encoderPos - prev;
		vel = (2 * 3141.59*diff) / (PPR*time);
		rpm = vel*9.55;
		angle = (double)encoderPos*90/PPR;
		prevtime = prevtime + time;
		prev = encoderPos;
	}
}



int Encoder::getDirection()
{
	return direction;
}

int Encoder::getEncoderPos()
{
	return encoderPos;
}

double Encoder::getRPM()
{
	return rpm;
}


double Encoder::getAngle()
{
	return angle;
}


double Encoder::getAngularVel()
{
	return vel;
}
