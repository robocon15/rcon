#include <Arduino.h>
#include <Servo.h>
#include <motor.h>
#include <twowheelbase.h>
#include <EncoderPID.h>
#include <EncoderTest.h>
#include <manualbot.h>
#include <PS2X_lib.h>
#include <maxconfig.h>

Fourwheelbaseps2 test;

void setup()
{
    Serial.begin(9600);
    test.attachFourBaseCurrent( DIR1_PINF, DIR2_PINF,PWM_PINF,DIR1_PINB,DIR2_PINB,PWM_PINB,DIR1_PINL, DIR2_PINL,PWM_PINL,DIR1_PINR, DIR2_PINR,PWM_PINR);
    //  test.attachPS2(CLOCK,COMMAND,ATTENTION,DATA);
    test.setMeanSpeed(255,255,255,255);
    test.attachPneumatic(P1_PIN1,P1_PIN2);
    test.attachService(DIR1_PIN1,DIR2_PIN1,PWM_PIN1,4);
    test.InitialiseVars();
    Serial.println("Starting");
}

void loop()
{
    //  test.run();
    test.run_laptop();
 }

