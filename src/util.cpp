#include "../include/util.h"

using namespace cv;
using namespace std;

//============================ Custom Functions ===============================//

double getDist(int pDepth) {
  return (-1)* 7.5 / ( tanf(0.0002157 * pDepth - 0.2356) );
}

double getDistAccurate(int pDepth) {
  return (-1)* 7.5 / ( tan(0.0002157 * pDepth - 0.2356) );
}

double getFPS() {
  static double t = 0;
  t = ((double)getTickCount() - t)/getTickFrequency();
  double p = 1.0/t;
  t = (double)getTickCount();
  return p;
}

void printFPS(Mat& img) {
  static double t = 0;
  t = ((double)getTickCount() - t)/getTickFrequency();
  t = 1.0/t;

  stringstream FPS;
  FPS << t;
  putText(img, FPS.str(), Point(260,460), 1, 2, Scalar(0,255,255), 2, 2, 0);

  t = (double)getTickCount();
}

Point3f coordinate(Point pt, InputArray _depthMat, double THETA) {
  Mat depthMat = _depthMat.getMat();
  double min_depth = min_depth = depthMat.at<uint16_t>(pt.y, pt.x);
  double focal = 6.09275494959; 
  double z = ((-1)* 7.5 / ( tanf(0.0002157 * min_depth  - 0.2356)))/100.0;
  double x = -(z * (pt.x - 320) * (-1) / focal)/100.0;
  double y = (z * (pt.y - 240) * (-1) / focal)/100.0;
  return Point3f(x, z*sin(THETA) + y*cos(THETA), z*cos(THETA) - y*sin(THETA));
}

void print_int_on_screen(Mat& img, int integer, int x, int y) {
  stringstream data;
  data << integer;
  putText(img, data.str(), Point(x,y), 1, 2, Scalar(0,255,255), 2, 2, 0);
}

void print_double_on_screen(Mat& img, double doubler, int x, int y) {
  stringstream data;
  data << doubler;
  putText(img, data.str(), Point(x,y), 1, 2, Scalar(0,255,255), 2, 2, 0);
}

void closestPt(InputArray _img, Point *pt) {
  // img is 16UC1
  Mat img = _img.getMat();
  minMaxLoc(img, NULL, NULL, pt, NULL);
}

// Takes a point in the depth image and returns the
// corresponding point in the rgb image
Point pt_rgb(Point pt_d, InputArray _depthMat){

  Mat depthMat = _depthMat.getMat();

  float fx_rgb = 5.2921508098293293e+02;
  float fy_rgb = 5.2556393630057437e+02;
  float cx_rgb = 3.2894272028759258e+02;
  float cy_rgb = 2.6748068171871557e+02;
  float k1_rgb = 2.6451622333009589e-01;
  float k2_rgb = -8.3990749424620825e-01;
  float p1_rgb = -1.9922302173693159e-03;
  float p2_rgb = 1.4371995932897616e-03;
  float k3_rgb = 9.1192465078713847e-01;

  float fx_d = 5.9421434211923247e+02;
  float fy_d = 5.9104053696870778e+02;
  float cx_d = 3.3930780975300314e+02;
  float cy_d = 2.4273913761751615e+02;
  float k1_d = -2.6386489753128833e-01;
  float k2_d = 9.9966832163729757e-01;
  float p1_d = -7.6275862143610667e-04;
  float p2_d = 5.0350940090814270e-03;
  float k3_d = -1.3053628089976321e+00;

  Mat R = *(Mat_<float>(3,3) <<  9.9984628826577793e-01, 1.2635359098409581e-03, -1.7487233004436643e-02,\
	    -1.477909610836448e-03, 9.9992385683542895e-01, -1.2251380107679535e-02,\
	    1.7470421412464927e-02, 1.2275341476520762e-02, 9.9977202419716948e-01); 
  Mat T = *(Mat_<float>(3,1) <<  1.9985242312092553e-02, -7.4423738761617583e-04, -1.0916736334336222e-02);

  int x_d = pt_d.x;
  int y_d = pt_d.y;

  int pdepth = depthMat.at<uint16_t>(x_d, y_d);

  Point3f P3D;
  P3D.z = getDist(pdepth);
  P3D.x = ((x_d - cx_d) * P3D.z)/(1.0*fx_d);
  P3D.y = ((y_d - cy_d) * P3D.z)/(1.0*fy_d);

  Mat p3d = *(Mat_<float>(3,1) << P3D.x, P3D.y, P3D.z);

  Mat p3d_;
  p3d_ = R*p3d + T;

  Point P2D;
  P2D.x = (int)((p3d_.at<float>(0) * fx_rgb / p3d_.at<float>(2)) + cx_rgb);
  P2D.y = (int)((p3d_.at<float>(1) * fy_rgb / p3d_.at<float>(2)) + cy_rgb);
	
  return P2D;
}
