# FILTERS = bin/cluster.o bin/canny_hough.o bin/motion.o bin/optflow.o bin/color.o bin/neighbourPt.o bin/blob.o
OBJECTS = bin/util.o bin/kinect.o bin/main.o bin/kalman.o bin/debug.o bin/controller.o # bin/clean.o $(FILTERS)
LDFLAGS = -lpthread -lfreenect -larmadillo `pkg-config --libs opencv`
LDDIR   = -I/usr/include/libfreenect -I/usr/local/include/libfreenect -I/usr/include/eigen3 -L/usr/local/cuda/lib64

main: $(OBJECTS)
	g++ $(OBJECTS) -o main $(LDDIR) $(LDFLAGS) 

bin/main.o: src/main.cpp
	g++ -c src/main.cpp -o bin/main.o -I /usr/include/libusb-1.0/ -I /usr/include/libfreenect/

bin/kinect.o: src/kinect.cpp
	g++ -c src/kinect.cpp -o bin/kinect.o -I /usr/include/libusb-1.0/ -I /usr/include/libfreenect/

bin/kalman.o: src/kalman.cpp
	g++ -c src/kalman.cpp -o bin/kalman.o

bin/%.o: src/%.cpp
	g++ -c $? -o $@ 

%: src/%.cpp
	g++ $(OBJECTS) -o $@ $(LDDIR) $(LDFLAGS)

clean:
	rm -rf bin/* main

clobber:
	rm -rf bin/* main test/bin/* test/*_test
