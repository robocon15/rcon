#include "../include/blob.h"

SimpleBlobDetector::Params set_params() {
  SimpleBlobDetector::Params params;
  params.minDistBetweenBlobs = 1.0f;
  params.filterByInertia = false;
  params.minInertiaRatio = 0.0f;
  params.maxInertiaRatio = 1.0f;
  params.minRepeatability = 2;
  params.filterByConvexity = false;
  params.filterByColor = false;
  params.blobColor = 100;
  params.filterByCircularity = false;
  params.minCircularity = 0.9f;
  params.maxCircularity = 1.0f;
  params.filterByArea = true;
  params.minArea = 5.0f;
  params.maxArea = 300.0f;
  return params;
}

void get_pts(Mat img, Ptr<FeatureDetector> blobDetector, vector<Point> *pts, vector<int> *score) {
  vector<KeyPoint> KP;
  blobDetector->detect(img, KP);
  for(int i=0; i<KP.size(); i++) {
    pts->push_back(KP[i].pt);
    score->push_back(0);
  }
}


//Ptr<FeatureDetector> blobDetector = new SimpleBlobDetector(params);
//blobDetector->detect(rgb, KP);
