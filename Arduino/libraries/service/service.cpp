#include <Arduino.h>
#include "service.h"

Service::Service()
{
	angle1 = 0;
	b = 0;
	frspeed = 50;
	initialAngle = 0;
	counter = 0;
}
Service::Service(int p1,int p2, int p3,int p4,int p5,int p6)
{
	attachServicePins(p1, p2, p3, p4, p5, p6);
}

void Service::attachServicePins(int p1, int p2, int p3, int p4, int p5, int p6)
{
	myservo.attach(p1);
	test.attachPins(p2, p3);
	m1.attachMotor(p5, p4, p6);
}

void Service::setup1()
{
	myservo.write(100);
    initialAngle = (int)(test.getAngle()/1.4713+36000000)%360;
}

void Service::runSerialinput(int input)
{
	//input=Serial.read();
    if(input=='1')
    {  
      myservo.write(140);
      delay(220);
	  m1.moveMotor(-1 * frspeed);
      delay(90);
    }
    else if(input=='2')
    {
      myservo.write(140);
      delay(220);
      //delay(5000);
      m1.moveMotor(frspeed);
      delay(90);
    }
    else if(input=='0')
    {
      m1.freeMotor();
      b=1;
      myservo.write(100); 
      angle1=0;
    }
}

void Service::serveRun()
{
	angle1=((int)(test.getAngle()/1.4713+36000000 - initialAngle))%360;
	//Serial.println(angle1);

  if(angle1>50 && b==0)
  {
    /* Serial.print(angle1);
     Serial.print(" ");
     Serial.print(test.getDirection());
     Serial.print(" ");
     Serial.print(initialAngle);
     Serial.print(" ");
     Serial.print(getRPM);
     Serial.print(" ");
     Serial.println(frspeed);*/

    if(angle1<=210)
    { 
      frspeed=-30;
      m1.moveMotor(-1*frspeed);
      b=1;
    }

  }
  if(b==1)
  {
    /* Serial.print(angle1);
     Serial.print(" ");
     Serial.print(test.getDirection());
     Serial.print(" ");
     Serial.print(initialAngle);
     Serial.print(" ");
     Serial.print(test.getRPM());
     Serial.print(" ");
     Serial.println(frspeed);*/

    if(angle1>350 || angle1<10)
    {
      frspeed=0;
    }
    else if(angle1>180)
      frspeed = angle1/2-360;
    else
      frspeed = angle1/2;
    m1.moveMotor(-1*frspeed);

  }
  //  Serial.println(test.getDirection());
  //  Serial.println(test.getRPM());
  //   Serial.println(test.getAngularVel());

}

int Service::getServiceAngle()
{
	
  int a= (int)(test.getAngle()/1.4713+36000000 - initialAngle);

  return a%360;
}

void Service::InterruptService()
{
  test.Interrupt();                    
}
