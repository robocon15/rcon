#include <manualbot.h>

Fourwheelbaseps2::Fourwheelbaseps2()
{
	attachFourBaseCurrent(-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1);
	setMeanSpeed(-1,-1,-1,-1);
	error = 0; 
	type = 0;
	vibrate=0;
	speed_bot=80;
	x=0;
	y=0;
	a=0;
	b=0;
	flag=0;
	flagP = 0;
}

void Fourwheelbaseps2::attachFourBaseCurrent(int Dir_pinf1,int Dir_pinf2,int Pwm_pinf,int Dir_pinb1,int Dir_pinb2,int Pwm_pinb,int Dir_pinl1,int Dir_pinl2,int Pwm_pinl,int Dir_pinr1,int Dir_pinr2,int Pwm_pinr)
{
	attachFourBase(Dir_pinf1,Dir_pinf2,Pwm_pinf,Dir_pinb1,Dir_pinb2,Pwm_pinb,Dir_pinl1,Dir_pinl2,Pwm_pinl,Dir_pinr1,Dir_pinr2,Pwm_pinr);
}

void Fourwheelbaseps2::attachService(int p1, int p2, int p3, int p4)
{
	Serve1=p1;
	Serve2=p2;
	ServePWM=p3;
	Servo=p4;
	pinMode(Serve1,OUTPUT);
	pinMode(Serve2,OUTPUT);
	pinMode(ServePWM,OUTPUT);
	attach(Servo);
	write(35);
}

void Fourwheelbaseps2::attachPneumatic(int pneu1, int pneu2)
{
	Pneu1 = pneu1;
	Pneu2 = pneu2;
	pinMode(Pneu1, OUTPUT);
	pinMode(Pneu2, OUTPUT);
	digitalWrite(Pneu1, LOW);
	digitalWrite(Pneu2, HIGH);
}



void Fourwheelbaseps2::attachPS2(uint8_t clk, uint8_t cmd, uint8_t att, uint8_t dat)
{
	error = config_gamepad(clk,cmd,att,dat,true,true);   //setup pins and settings:  GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error

	if(error == 0)
	{
		Serial.println("Found Controller, configured successful");
		Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
		Serial.println("holding L1 or R1 will print out the analog stick values.");
		Serial.println("Go to www.billporter.info for updates and to report bugs.");
	}

	else if(error == 1)
		Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");

	else if(error == 2)
		Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");

	else if(error == 3)
		Serial.println("Controller refusing to enter Pressures mode, may not support it. ");

	//Serial.print(ps2x.Analog(1), HEX);

	//type = ps2x.readType(); 
	type = readType();
	switch(type) 
	{
		case 0:
			Serial.println("Unknown Controller type");
			break;
		case 1:
			Serial.println("DualShock Controller Found");
			break;
		case 2:
			Serial.println("GuitarHero Controller Found");
			break;
	}
}

void Fourwheelbaseps2::setMeanSpeed(int Meanspeedf,int Meanspeedb,int Meanspeedl,int Meanspeedr)
{
	meanSpeedCurrent(Meanspeedf,Meanspeedb,Meanspeedl,Meanspeedr);
}

int Fourwheelbaseps2::calc(int x_y)
{
	return x_y-CORDINATE_CONST;
}



void Fourwheelbaseps2::run()
{
	if(type==1)
	{
		read_gamepad(false, vibrate);
		x=calc(Analog(PSS_LX));
		y=-calc(Analog(PSS_LY));
		a=calc(Analog(PSS_RX));
		b=-calc(Analog(PSS_RY));
		// Serial.print(x );
		// Serial.print(",  " );
		// Serial.print(y );
		// Serial.print(",  " );
		// Serial.print(a);
		// Serial.print(",  " );
		// Serial.println(b );

		if(x==255-CORDINATE_CONST && -y==255-CORDINATE_CONST && a==255-CORDINATE_CONST && -b==255-CORDINATE_CONST)
			Serial.println("Analog is probably off");
		else if(abs(x)<45&&abs(y)<45&&abs(a)<45&&abs(b)<45&&!Button(PSB_L2) && !Button(PSB_R2))
		{
			freeMotorCurrent(1,1);
			Serial.println("no button pressed");

		}
		else if(abs(x)>45||abs(y)>45)
		{
			if((y-((127.0/45.0)*x)>0)&&(y+((127.0/45.0)*x)>0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y>0)
			{
				int Speed=map(y,-127,127,-speed_bot,speed_bot);
				botForward(Speed);
				Serial.println("move forward ");
			}

			else if((y-((127.0/45.0)*x)<0)&&(y+((127.0/45.0)*x)<0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y<0)
			{
				int Speed=map(y,-127,127,-speed_bot,speed_bot);
				botForward(Speed);
				Serial.println("move  backward");
			}

			else if((y-((45.0/128.0)*x)<0)&&(y+((45.0/128.0)*x)>0)&&!Button(PSB_L2) && !Button(PSB_R2) &&x>0)
			{
				int Speed=map(x,-127,127,-speed_bot,speed_bot);
				botRight(Speed);
				Serial.println("move  right");
			}

			else if((y-((45.0/128.0)*x)>0)&&(y+((45.0/128.0)*x)<0)&&!Button(PSB_L2) && !Button(PSB_R2)&&x<0)
			{
				int Speed=map(x,-127,127,-speed_bot,speed_bot);
				botRight(Speed);
				Serial.println("move left ");
			}

			else if((y-((127.0/45.0)*x)<0)&&(y-((127.0/76.0)*x)>0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(30,Speed);
				Serial.println("move 30 forward");
			}

			else if((y-((127.0/45.0)*x)>0)&&(y-((127.0/76.0)*x)<0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(30,Speed);
				Serial.println("move 30 Backward");
			}

			else if((y-((63.0/128.0)*x)>0)&&(y-((127.0/76.0)*x)<0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(45,Speed);
				Serial.println("move 45 forward");
			}

			else if((y-((63.0/128.0)*x)<0)&&(y-((127.0/76.0)*x)>0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(45,Speed);
				Serial.println("move 45 backard");
			}

			else if((y-((63.0/128.0)*x)<0)&&(y-((45.0/128.0)*x)>0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(60,Speed);
				Serial.println("move 60 forward");
			}

			else if((y-((63.0/128.0)*x)>0)&&(y-((45.0/128.0)*x)<0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(60,Speed);
				Serial.println("move 60 backward");
			}

			else if((y+((85.0/128.0)*x)>0)&&(y+((45.0/128.0)*x)<0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-60,Speed);
				Serial.println("move -60 backward");
			}

			else if((y+((85.0/128.0)*x)<0)&&(y+((45.0/128.0)*x)>0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-60,Speed);
				Serial.println("move -60 forward");
			}

			else if((y+((85.0/128.0)*x)<0)&&(y+((128.0/90.0)*x)>0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-45,Speed);
				Serial.println("-45 backward");
			}

			else if((y+((85.0/128.0)*x)>0)&&(y+((128.0/90.0)*x)<0)&&!Button(PSB_L2) && !Button(PSB_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-45,Speed);
				Serial.println("-45 forward");
			}

			else if((y+((128.0/90.0)*x)<0)&&(y+((127.0/45.0)*x)>0)&&!Button(PSB_L2) && !Button(PSB_R2) && y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-30,Speed);
				Serial.println("move -30 backward");
			}

			else if((y+((128.0/90.0)*x)>0)&&(y+((127.0/45.0)*x)<0)&&!Button(PSB_L2) && !Button(PSB_R2)&& y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-30,Speed);
				Serial.println("move -30 forward");
			}
		}
		if(a<-45&&!Button(PSB_L2) && !Button(PSB_R2))
		{
			float d=sqrt(a*a + b*b);
			float Speed=map(d,-127,127,-speed_bot,speed_bot);
			rotateAntiClockSpeed(Speed/3);
			Serial.println("move anticlock");
		}
		if(a>45&&!Button(PSB_L2) && !Button(PSB_R2))
		{
			float d=sqrt(a*a + b*b);
			float Speed=map(d,-127,127,-speed_bot,speed_bot);
			rotateClockSpeed(Speed/3);
			Serial.println("move clock");
		}
		else if(Button(PSB_L2) | Button(PSB_R2))
		{
			stopBot();
			speed_bot=50;
			Serial.println("bumper ahead");
		}


		if((ButtonPressed(PSB_L1) | ButtonPressed(PSB_R1))& !flag )
		{
			speed_bot=speed_bot+ 10*(ButtonPressed(PSB_L1)-ButtonPressed(PSB_R1));
			if(abs(x)<45&&abs(y)<45)
				flag=1;
		}
		if(abs(x)>45||abs(y)<45)
			flag=0;


		if (ButtonPressed(PSB_GREEN))
		{
			Serial.println("Hitting");
			if (flagP == 0)
			{
				digitalWrite(Pneu1, HIGH);
				digitalWrite(Pneu2, LOW);

				flagP = 1;
			}
			delay(500);
			if (flagP == 1)
			{
				digitalWrite(Pneu1, LOW);
				digitalWrite(Pneu2, HIGH);
				flagP = 0;
			}
		}

		//		if (ButtonPressed(PSB_RED))
		//		{
		//			//Serial.println("Hitting");
		//			if (flagP == 1)
		//			{
		//				digitalWrite(Pneu1, LOW);
		//				digitalWrite(Pneu2, HIGH);
		//
		//				flagP = 0;
		//	
		//            }
		//		}

		write(35);

		if (ButtonPressed(PSB_RED))
		{
			Serial.println("Service");
			// write(180);

			digitalWrite(Serve1,HIGH);
			digitalWrite(Serve2,LOW);
			analogWrite(ServePWM,200);
			delay(200);

			write(60);
			delay(1000);

			digitalWrite(Serve1,LOW);
			digitalWrite(Serve2,LOW);
			analogWrite(ServePWM,0);

			write(35);
		}  
		if (ButtonPressed(PSB_BLUE))
		{
			Serial.println("random hit");
			// write(180);

			digitalWrite(Serve1,HIGH);
			digitalWrite(Serve2,LOW);
			analogWrite(ServePWM,200);


		}
		if(ButtonReleased(PSB_BLUE))
		{
			digitalWrite(Serve1,LOW);
			digitalWrite(Serve2,LOW);
			analogWrite(ServePWM,0);
		}
	}

}

int LButtonArr[7];
int LButtonOld[7];
void Fourwheelbaseps2::InitialiseVars()
{
	int i;
	for(i=0;i<7;i++)
		LButtonOld[i] = 0;
}

bool Fourwheelbaseps2::LButton(int index)
{
	if(index == 7)
		return 0;
	return LButtonArr[index];
}

bool Fourwheelbaseps2::LButtonPressed(int index)
{
	return  (LButtonArr[index] && !LButtonOld[index]);
}

bool Fourwheelbaseps2::LButtonReleased(int index)
{
	return (!LButtonArr[index] && LButtonOld[index]);
}

void Fourwheelbaseps2::run_laptop()
{
	while(!Serial.available());
	if(Serial.read() == 12)
	{
		char arr[4];
		Serial.readBytes(arr, 4);

		int x = arr[0];
		int y = arr[1];
		int a = arr[2];
		//int b = arr[3];
		int b = 0;
		int LButtonInputChar = arr[3];

		x = -x;

		int i;
		for(i=0;i<4;i++)
			if((int)arr[i] == 12 )
			{
				Serial.println("___SKIPPING___");
				//break;
				return;
			}

		for(i=0; i<7; i++)
		{
			LButtonOld[i] = LButtonArr[i];
			LButtonArr[i] = LButtonInputChar%2;
			LButtonInputChar /= 2;
		}


		if(x==255-CORDINATE_CONST && -y==255-CORDINATE_CONST && a==255-CORDINATE_CONST && -b==255-CORDINATE_CONST)
			Serial.println("Analog is probably off");
		else if(abs(x)<45 && abs(y)<45 && abs(a)<45 && abs(b)<45 && !LButton(PSL_L2) && !LButton(PSL_R2))
		{
			freeMotorCurrent(1,1);
			//Serial.println("no button pressed");
		}
		else if(abs(x)>45||abs(y)>45)
		{
			if((y-((127.0/45.0)*x)>0)&&(y+((127.0/45.0)*x)>0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y>0)
			{
				int Speed=map(y,-127,127,-speed_bot,speed_bot);
				botForward(Speed);
				Serial.println("move forward ");
			}

			else if((y-((127.0/45.0)*x)<0)&&(y+((127.0/45.0)*x)<0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y<0)
			{
				int Speed=map(y,-127,127,-speed_bot,speed_bot);
				botForward(Speed);
				Serial.println("move  backward");
			}

			else if((y-((45.0/128.0)*x)<0)&&(y+((45.0/128.0)*x)>0)&&!LButton(PSL_L2) && !LButton(PSL_R2) &&x>0)
			{
				int Speed=map(x,-127,127,-speed_bot,speed_bot);
				botRight(Speed);
				Serial.println("move right");
			}

			else if((y-((45.0/128.0)*x)>0)&&(y+((45.0/128.0)*x)<0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&x<0)
			{
				int Speed=map(x,-127,127,-speed_bot,speed_bot);
				botRight(Speed);
				Serial.println("move left");
			}

			else if((y-((127.0/45.0)*x)<0)&&(y-((127.0/76.0)*x)>0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(30,Speed);
				Serial.println("move 30 forward");
			}

			else if((y-((127.0/45.0)*x)>0)&&(y-((127.0/76.0)*x)<0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(30,Speed);
				Serial.println("move 30 Backward");
			}

			else if((y-((63.0/128.0)*x)>0)&&(y-((127.0/76.0)*x)<0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(45,Speed);
				Serial.println("move 45 forward");
			}

			else if((y-((63.0/128.0)*x)<0)&&(y-((127.0/76.0)*x)>0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(45,Speed);
				Serial.println("move 45 backard");
			}

			else if((y-((63.0/128.0)*x)<0)&&(y-((45.0/128.0)*x)>0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(60,Speed);
				Serial.println("move 60 forward");
			}

			else if((y-((63.0/128.0)*x)>0)&&(y-((45.0/128.0)*x)<0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(x<0)
					Speed=-Speed;
				transAngle(60,Speed);
				Serial.println("move 60 backward");
			}

			else if((y+((85.0/128.0)*x)>0)&&(y+((45.0/128.0)*x)<0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-60,Speed);
				Serial.println("move -60 backward");
			}

			else if((y+((85.0/128.0)*x)<0)&&(y+((45.0/128.0)*x)>0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-60,Speed);
				Serial.println("move -60 forward");
			}

			else if((y+((85.0/128.0)*x)<0)&&(y+((128.0/90.0)*x)>0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-45,Speed);
				Serial.println("-45 backward");
			}

			else if((y+((85.0/128.0)*x)>0)&&(y+((128.0/90.0)*x)<0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&&y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-45,Speed);
				Serial.println("-45 forward");
			}

			else if((y+((128.0/90.0)*x)<0)&&(y+((127.0/45.0)*x)>0)&&!LButton(PSL_L2) && !LButton(PSL_R2) && y<0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-30,Speed);
				Serial.println("move -30 backward");
			}

			else if((y+((128.0/90.0)*x)>0)&&(y+((127.0/45.0)*x)<0)&&!LButton(PSL_L2) && !LButton(PSL_R2)&& y>0)
			{
				float d=sqrt(x*x + y*y);
				float Speed=map(d,-127,127,-speed_bot,speed_bot);
				if(y<0)
					Speed=-Speed;
				transAngle(-30,Speed);
				Serial.println("move -30 forward");
			}
		}
		if(a<-45&&!LButton(PSL_L2) && !LButton(PSL_R2))
		{
			float d=sqrt(a*a + b*b);
			float Speed=map(d,-127,127,-speed_bot,speed_bot);
			rotateAntiClockSpeed(Speed/3);
			Serial.println("move anticlock");
		}
		if(a>45&&!LButton(PSL_L2) && !LButton(PSL_R2))
		{
			float d=sqrt(a*a + b*b);
			float Speed=map(d,-127,127,-speed_bot,speed_bot);
			rotateClockSpeed(Speed/3);
			Serial.println("move clock");
		}
		else if(LButton(PSL_R2))
		{
			stopBot();
			speed_bot=50;
			Serial.println("bumper ahead");
		}


		if((LButtonPressed(PSL_L1) | LButtonPressed(PSL_R1))& !flag )
		{
			Serial.println("PWM changed");
			speed_bot=speed_bot+ 10*(LButtonPressed(PSL_L1)-LButtonPressed(PSL_R1));
			if(abs(x)<45&&abs(y)<45)
				flag=1;
		}
		if(abs(x)>45||abs(y)<45)
			flag=0;


		if (LButtonPressed(PSL_GREEN))
		{
			Serial.println("Hitting");
			if (flagP == 0)
			{
				digitalWrite(Pneu1, HIGH);
				digitalWrite(Pneu2, LOW);

				flagP = 1;
			}
			delay(500);
			if (flagP == 1)
			{
				digitalWrite(Pneu1, LOW);
				digitalWrite(Pneu2, HIGH);
				flagP = 0;
			}
		}

		write(35);

		//if(LButtonArr[PSL_RED])
		//Serial.println("Red pressed");

		if (LButtonReleased(PSL_RED))
		{
			Serial.println("Service");
			// write(180);

			digitalWrite(Serve1,HIGH);
			digitalWrite(Serve2,LOW);
			analogWrite(ServePWM,200);
			delay(200);

			write(60);
			delay(1000);

			digitalWrite(Serve1,LOW);
			digitalWrite(Serve2,LOW);
			analogWrite(ServePWM,0);

			write(35);
		}  
		if (LButtonPressed(PSL_BLUE))
		{
			Serial.println("random hit");
			// write(180);

			digitalWrite(Serve1,HIGH);
			digitalWrite(Serve2,LOW);
			analogWrite(ServePWM,200);


		}
		if(LButtonReleased(PSL_BLUE))
		{
			digitalWrite(Serve1,LOW);
			digitalWrite(Serve2,LOW);
			analogWrite(ServePWM,0);
		}
	}
}
