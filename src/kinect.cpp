/*
  Author: Saksham Sharma
  Date:	11 Dec, 2014	(well, it's 12th Dec now :P)
*/

#include "../include/kinect.h"

using namespace cv;
using namespace std;

myMutex::myMutex() {
  pthread_mutex_init( &m_mutex, NULL );
}
void myMutex::lock() {
  pthread_mutex_lock( &m_mutex );
}
void myMutex::unlock() {
  pthread_mutex_unlock( &m_mutex );
}
 
//=========================== FREENECT Functions ==============================//
void MyFreenectDevice::VideoCallback(void* _rgb, uint32_t timestamp) {
  m_rgb_mutex.lock();
  uint8_t* rgb = static_cast<uint8_t*>(_rgb);
  rgbMat.data = rgb;
  m_new_rgb_frame = true;
  m_rgb_mutex.unlock();
};

void MyFreenectDevice::DepthCallback(void* _depth, uint32_t timestamp) {
  m_depth_mutex.lock();
  uint16_t* depth = static_cast<uint16_t*>(_depth);
  depthMat.data = (uchar*) depth;
  m_new_depth_frame = true;
  m_depth_mutex.unlock();
}

bool MyFreenectDevice::getVideo(Mat& output) {
  m_rgb_mutex.lock();
  if(m_new_rgb_frame) {
    cv::cvtColor(rgbMat, output, CV_RGB2BGR);
    m_new_rgb_frame = false;
    m_rgb_mutex.unlock();
    return true;
  } else {
    m_rgb_mutex.unlock();
    return false;
  }
}

bool MyFreenectDevice::getDepth(Mat& output) {
  m_depth_mutex.lock();
  if(m_new_depth_frame) {
    depthMat.copyTo(output);
    m_new_depth_frame = false;
    m_depth_mutex.unlock();
    return true;
  } else {
    m_depth_mutex.unlock();
    return false;
  }
}

double THETA;
double focal = 6.09275494959; 
