#ifndef _BLOB_H_
#define _BLOB_H_

#include <opencv2/opencv.hpp>
using namespace cv;


#define init_blob(_params, _blobDetector) SimpleBlobDetector::Params _params = set_params();\
        Ptr<FeatureDetector> _blobDetector = new SimpleBlobDetector(_params)

void get_pts(Mat img, Ptr<FeatureDetector> blobDetector, vector<Point> *pts, vector<int> *score);

SimpleBlobDetector::Params set_params();

#endif
