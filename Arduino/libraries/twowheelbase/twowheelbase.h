/*library started on 30th November
  Made by Zaid Tasneem
*/
/* tested on 3rd dec */
#include <motor.h>
//#include <botconfig.h>
#include <Arduino.h>

#ifndef  TWO_WHEEL_BASE_H
#define  TWO_WHEEL_BASE_H

//
//
//
class Twowheelbase : public Motor
{
        Motor left_motor;
		Motor right_motor;
		
// Functions
     public:
	 Twowheelbase();																	//constructors
	 Twowheelbase(int Ldir1, int Ldir2 , int Lpwm, int Rdir1, int Rdir2, int Rpwm);		//parametrised constructors
	 void attachBase(int Ldir1, int Ldir2 , int Lpwm, int Rdir1, int Rdir2, int Rpwm);  //by default meanspeed is 255
	 
	 void setMeanSpeed(int Lmeanspeed, int Rmeanspeed);									//Speed refers to percentage of Meanspeed 
	 
	 void moveForward(float Speed); 													// Speed is in 0-100%
	 void moveForward(float Lspeed, float Rspeed); 										//speed of each wheel can be set individually as the bot move forward
	 void moveBackward(float Speed);													//Speed only positive
	 void moveBackward(float Lspeed, float Rspeed); 									//speed of each wheel can be set individually as the bot move backwards
	 
	 void rotate(float Speed);															//Dir +ve for C.W. and vice versa
	 void rotateRight(float Speed);    													//rotates c.w. Speed is 0-100%
	 void rotateRight(float Lspeed, float Rspeed);
	 void rotateLeft(float Speed);
	 void rotateLeft(float Lspeed, float Rspeed);
	 
	 void stop();																		//it locks the motor instantaneously
	 void stopSmoothly();																
	 void startSmoothly(float Speed);
	 void startSmoothly(float Lspeed,float Rspeed);
	 void freeWheels();
	 //void coverDistanceSmoothly(float Distance);
	 //void coverDistanceSuddenly(float Distance);
	 //void traverseCurve(float Radius, float Speed);
};
#endif
