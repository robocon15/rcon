#include "../include/kalman.h"
int xKalMove = 0;
int yKalMove = 0;

float k   = 0.0006;
float m   = 0.005;		//	SI UNITS
float g_x = 0;
float g_y = 9.80665;
float g_z = 0;
float dt  = 0.04;

double rel_vel_x = 0;
double rel_vel_z = 0;
double rel_pos_x = 0;
double rel_pos_z = 0;
double cur_vel_x = 0;
double cur_vel_z = 0;

double x = 0;
double y = 0;
double z = 0;
double v_x  = 0;
double v_y  = 0;
double v_z  = 0;
double old_x  = 999.999;
double old_y  = 999.999;
double old_z  = 999.999;
bool Kalman_flag = 0;

long timePred = 99999999999999999999999999;
int countTimes = 0;

arma::mat A_x = arma::mat(2,2,arma::fill::eye);
arma::mat P_x = arma::mat(2,2,arma::fill::eye);
arma::mat Q_x = arma::mat(2,2,arma::fill::eye);
arma::mat B_x = arma::mat(2,2,arma::fill::eye);
arma::mat H_x = arma::mat(2,2,arma::fill::eye);
arma::mat R_x = arma::mat(2,2,arma::fill::eye);
arma::mat X_x = arma::mat(2,1,arma::fill::zeros);
arma::mat U_x = arma::mat(2,1,arma::fill::zeros); 

arma::mat A_y = arma::mat(2,2,arma::fill::eye);
arma::mat P_y = arma::mat(2,2,arma::fill::eye);
arma::mat Q_y = arma::mat(2,2,arma::fill::eye);
arma::mat B_y = arma::mat(2,2,arma::fill::eye);
arma::mat H_y = arma::mat(2,2,arma::fill::eye);
arma::mat R_y = arma::mat(2,2,arma::fill::eye);
arma::mat X_y = arma::mat(2,1,arma::fill::zeros);
arma::mat U_y = arma::mat(2,1,arma::fill::zeros); 

arma::mat A_z = arma::mat(2,2,arma::fill::eye);
arma::mat P_z = arma::mat(2,2,arma::fill::eye);
arma::mat Q_z = arma::mat(2,2,arma::fill::eye);
arma::mat B_z = arma::mat(2,2,arma::fill::eye);
arma::mat H_z = arma::mat(2,2,arma::fill::eye);
arma::mat R_z = arma::mat(2,2,arma::fill::eye);
arma::mat X_z = arma::mat(2,1,arma::fill::zeros);
arma::mat U_z = arma::mat(2,1,arma::fill::zeros); 

void Kalman_calc(double x, double v_x, double y, double v_y, double z, double v_z) {

	U_y 	<<	-0.5*g_y*dt*dt 		<< 	arma::endr
			<<	    -g_y*dt 		<<	arma::endr;
	A_x		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
	A_y		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
	A_z		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;


	arma::mat Z_x;
	arma::mat xPred_x;
	arma::mat pPred_x;
	arma::mat y_x;
	arma::mat S_x;
	arma::mat K_x;
	arma::mat I_x = arma::mat(2,2,arma::fill::eye);

	Z_x << x << arma::endr << v_x << arma::endr;

	xPred_x = A_x*X_x + B_x*U_x;
	pPred_x = A_x*P_x*arma::trans(A_x) + Q_x;
	y_x 	  = Z_x - H_x*xPred_x;
	S_x 	  = H_x*pPred_x*arma::trans(H_x) + R_x;
	K_x 	  = pPred_x*arma::trans(H_x)*arma::inv(S_x);
	X_x 	  = xPred_x + K_x*y_x;
	P_x 	  = (I_x - K_x*H_x)*pPred_x;


	arma::mat Z_y;
	arma::mat xPred_y;
	arma::mat pPred_y;
	arma::mat y_y;
	arma::mat S_y;
	arma::mat K_y;
	arma::mat I_y = arma::mat(2,2,arma::fill::eye);

	Z_y << y << arma::endr << v_y << arma::endr;

	xPred_y = A_y*X_y + B_y*U_y;
	pPred_y = A_y*P_y*arma::trans(A_y) + Q_y;
	y_y 	  = Z_y - H_y*xPred_y;
	S_y 	  = H_y*pPred_y*arma::trans(H_y) + R_y;
	K_y 	  = pPred_y*arma::trans(H_y)*arma::inv(S_y);
	X_y 	  = xPred_y + K_y*y_y;
	P_y 	  = (I_y - K_y*H_y)*pPred_y;


	arma::mat Z_z;
	arma::mat xPred_z;
	arma::mat pPred_z;
	arma::mat y_z;
	arma::mat S_z;
	arma::mat K_z;
	arma::mat I_z = arma::mat(2,2,arma::fill::eye);

	Z_z << z << arma::endr << v_z << arma::endr;

	xPred_z = A_z*X_z + B_z*U_z;
	pPred_z = A_z*P_z*arma::trans(A_z) + Q_z;
	y_z 	  = Z_z - H_z*xPred_z;
	S_z 	  = H_z*pPred_z*arma::trans(H_z) + R_z;
	K_z 	  = pPred_z*arma::trans(H_z)*arma::inv(S_z);
	X_z 	  = xPred_z + K_z*y_z;
	P_z 	  = (I_z - K_z*H_z)*pPred_z;
}

void resetKalman()
{
	reInitKalman(x);
	reInitKalman(y);
	reInitKalman(z);
	countTimes = 0;
	rel_pos_x = 0;
	rel_pos_z = 0;
	cur_vel_x = 0;
	cur_vel_z = 0;
	x 	= 0;
	y 	= 0;
	z 	= 0;
	v_x  = 0;
	v_y  = 0;
	v_z  = 0;
	old_x  = 999.999;
	old_y  = 999.999;
	old_z  = 999.999;
	Kalman_flag = 0;
    timePred = 99999999999999999999999999;
}

void predict(double DT, Mat RGB) {
	dt = DT;
	//  cout << dt << endl;
	countTimes++;
	int t=50;

	arma::mat Prediction_x = A_x*X_x + B_x*U_x;
	arma::mat Prediction_y = A_y*X_y + B_y*U_y;
	arma::mat Prediction_z = A_z*X_z + B_z*U_z;

    double timer=0;
    Point shuttleIntended = Point(0,0);
	while(t--) {
        timer += dt;
		Point shuttleIntended = Point(0,0);

		if(Prediction_y(0, 0) < 0.05 )/*&& t<40) * && Prediction_y(1, 0) < 0) */ {
			cout << "After " << countTimes << " iterations :\n";
			cout << "Expected Bot position is:" << rel_pos_x << ", " << rel_pos_z<<" .\n";

			shuttleIntended.x = abs( Prediction_x(0, 0) ) > 0.1 ? Prediction_x(0, 0)*100 : 0;
			shuttleIntended.y = abs( Prediction_z(0, 0) ) > 0.1 ? Prediction_z(0, 0)*100 : 0;

			if(!debug && countTimes==5)
			{
				int xer, yer; 
				xer = shuttleIntended.x > 0 ? 1 : -1 ;
				yer = shuttleIntended.y > 0 ? 1 : -1 ;
				
				if(shuttleIntended.x == 0 && shuttleIntended.y == 0)
					writeToArduino(0, 0);
				else if(shuttleIntended.x == 0 && shuttleIntended.y != 0)
					writeToArduino(0, yer);
				else if(shuttleIntended.y == 0 && shuttleIntended.x != 0)
					writeToArduino(xer, 0);
				else
				{
					if ( abs(shuttleIntended.x) > abs(shuttleIntended.y) )
						writeToArduino( xer, yer * abs(shuttleIntended.y)/abs(shuttleIntended.x) );
					else
						writeToArduino( xer * abs(shuttleIntended.x)/abs(shuttleIntended.y) , yer );
				}
			}


			if(Prediction_x(0, 0)-rel_pos_x > 0.1)
			{
				printf("Right\t%lf\n" , Prediction_x(0, 0)*100);
			}
			else if(Prediction_x(0, 0)-rel_pos_x < -0.1)
			{
				printf("Left\t%lf\n" , Prediction_x(0, 0)*100);
			}
			else 
			{
				printf("Stop\t%lf\n" , Prediction_x(0, 0)*100);
			}
			if(Prediction_z(0, 0)-rel_pos_z > 0.1)
			{
				printf("Front\t%lf\n" , Prediction_z(0, 0)*100);
			}
			else if(Prediction_z(0, 0)-rel_pos_z < -0.1)
			{
				printf("Back\t%lf\n" , Prediction_z(0, 0)*100);
			}
			else
			{
				printf("Stay\t%lf\n" , Prediction_z(0, 0)*100);
			}
			break;
		}
		Prediction_x = A_x*Prediction_x + B_x*U_x;
		Prediction_y = A_y*Prediction_y + B_y*U_y;
		Prediction_z = A_z*Prediction_z + B_z*U_z;
	}
	stringstream ss;
	ss << timer;
	if(countTimes==5)
	{
		timePred = cv::getTickCount()+getTickFrequency()*timer;
	}
	putText(RGB, ss.str(), Point(270,60), 3, 1, Scalar(255,0,255), 2, 8);
	stringstream ss2;
	ss2 << cv::getTickCount()/cv::getTickFrequency();
	stringstream ss3;
	ss3<<"img/"<<countTimes<<".jpg";
	putText(RGB, ss2.str(), Point(270,80), 3, 1, Scalar(255,0,255), 2, 8);
	imwrite(ss3.str(), RGB);
}

double tanF(double x) {
	return (x-x*x*x/3);
}

double get_dt() {
	// Returns the time between two frames
	double t2 = cv::getTickCount();
	double freq = cv::getTickFrequency();
	static double t1 = t2 - freq*0.04;
	double dt = ((t2-t1)/freq);
	t1 = t2;
	return dt;
}
