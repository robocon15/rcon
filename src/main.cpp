#include "../include/includes.h"

using namespace std;
using namespace cv;
using namespace Freenect;

Mat mapx(480, 640, CV_32FC1), mapy(480, 640, CV_32FC1);

int main() {

  // Point prevpt = Point(320, 240);
	// code to align images
	for(int i=0; i<640; i++) 
    {
		for(int j=0; j<480; j++) 
        {
			int alpha = i;
			int beta = j;
			int ax = 9.1008894 + 0.924429*i; //i;// (((alpha-310)*241)/256)+293;
			int ay = 40.0500822 + 0.92345789*j;// (((beta-210)*240)/256)+240;
			int val;
			if(ax<0 || ay<0 || ax>640 || ay>480)
				val=0;
			mapx.at<float>(j, i) = (int)ax;
			mapy.at<float>(j, i) = (int)ay;
		}
	}
    double preCalcDepth[2048];
    for(int i=0; i<2048; i++)
    {
        preCalcDepth[i] = ((-1)* 7.5 / ( tan(0.0002157 * i  - 0.2356)))/100.0;
    }
    Freenect::Freenect freenect;
    MyFreenectDevice& device = freenect.createDevice<MyFreenectDevice>(0);
    device.startVideo();
    device.startDepth();
    resetKalman();

    Mat /*oldDepthMat,*/ depthMat;
    Mat RGB, rgb,  depth,  gray /*,oldDepth, oldRgb, oldGray, depthGray, oldDepthGray*/;

    dt = get_dt();
    device.updateState();
    THETA = device.getState().getTiltDegs();

    printf("Kinect at an angle of %d Degrees\n", (int)THETA);
    THETA = 3.14159*THETA/180;	// Convert angle in Rads for use.

    // Mat depthMatPal(Size(640,480),CV_16UC1);
    // do {
    // 	device.getDepth(oldDepthMat);
    // 	device.getVideo(oldRgb);
    // } while (oldRgb.empty() || oldDepthMat.empty());
    // remap(oldRgb, oldRgb, mapx, mapy, INTER_LINEAR);
    // cvtColor(oldRgb, oldGray, COLOR_BGR2GRAY);

    // intializing joystick position
    jp[0].x = 0;
    jp[1].x = 0;
    jp[0].y = 0;
    jp[1].y = 0;
    //firstFixed = 1;

    cout << "Ready..." << endl;
    while (true) 
    {
        bool KalmanPred = true;
        stringstream joy;

        jp[0] = js.joystickPosition(0);
        jp[1] = js.joystickPosition(1);

        joy << jp[0].x << "," << jp[0].y;
        if( abs(jp[0].x) < 0.01 && abs(jp[0].y) < 0.01)
            ManualInput = 0;
        else
            ManualInput = 1;

        UpdateButtonState();

        int i=0;
        do {
            device.getDepth(depthMat);
            device.getVideo(rgb);
        } while (rgb.empty() || depthMat.empty());

        remap(rgb, rgb, mapx, mapy, INTER_LINEAR);

        // depthMatPal = depthMat.clone();

        cvtColor(rgb, gray, COLOR_BGR2GRAY);


        RGB = rgb.clone();

        //==============================Controller=================================//
        if(isControl)
            putText(RGB, joy.str(), Point(280,430), 1, 2, Scalar(0,255,255), 2, 2, 0);
        printFPS(RGB);

        //================================Kalman==================================//


        Rect roi = Rect(Point(30,35), Point(610,465)); // ignoring boundaries
        rectangle(RGB, roi, Scalar(100,255,150),2,8);

        int min, min_x, min_y;
        Point minLoc;
        double minVal;

        minMaxLoc(depthMat(roi), &minVal, NULL, &minLoc, NULL);
        minLoc += roi.tl(); // because minMaxLoc returns the coordinate  of point with min depth wrt roi
        min = (int) minVal;
        min_x = (int) minLoc.x;
        min_y = (int) minLoc.y;

        stringstream debugger;
        debugger << debug << " " << min;
        putText(RGB, debugger.str(), Point(270,30), 3, 1, Scalar(255,0,255), 2, 8);

        // prevpt = Point(min_x, min_y);
        z = preCalcDepth[min];
        x = -(z * (min_x - 320) * (-1) / focal)/100.0;
        y = (z * (min_y - 240) * (-1) / focal)/100.0;
        double z_temp = z*cos(THETA) - y*sin(THETA);
        double y_temp = z*sin(THETA) + y*cos(THETA);
        z = z_temp;							
        y = y_temp;							// x, y, z found wrt kinect
        z -= rel_pos_z;
        x -= rel_pos_x;

        if(z>4 || debug || z<0) 
        { 
            if(z>4)
                circle(RGB, Point(min_x, min_y), 20, Scalar(255, 0, 0), 5);
            else if(z<0)
                circle(RGB, Point(min_x, min_y), 20, Scalar(0, 255, 0), 5);
            else
            {
                circle(RGB, Point(min_x, min_y), 20, Scalar(255,255,0), 5);
            }
            KalmanPred = false;
        }

        //------------MAIN KALMAN CODE-----------//

        // Calculate velocity from coordinates.
        if (KalmanPred) 
        {
            v_x = (x - old_x)/dt;
            v_y = (y - old_y)/dt;
            v_z = (z - old_z)/dt;

            dt = get_dt();			// This moved from top of while to here
            rel_pos_x += cur_vel_x * dt;
            rel_pos_z += cur_vel_z * dt;

            if((abs(v_x>30) || abs(v_y)>30 || abs(v_z)>30 || abs(old_x - 999.999) < 0.01) && !Kalman_flag) 
            {
                old_x = x;
                old_y = y;
                old_z = z;
                KalmanPred = false;
            }
            else if(abs(v_x>30) || abs(v_y)>30 || abs(v_z)>30 || abs(old_x - 999.999) < 0.01)
            {
                // These lines copied to kalman.cpp also.
                U_y 	<<	-0.5*g_y*dt*dt 	        	<< 	arma::endr
                    <<	    -g_y*dt          		<<	arma::endr;
                A_x		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
                    <<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
                A_y		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
                    <<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
                A_z		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
                    <<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
                arma::mat xPred_x = A_x * X_x + B_x * U_x;
                arma::mat xPred_y = A_y * X_y + B_y * U_y;
                arma::mat xPred_z = A_z * X_z + B_z * U_z;
                x 	= xPred_x(0);
                v_x = xPred_x(1);
                y 	= xPred_y(0);
                v_y = xPred_y(1);
                z 	= xPred_z(0);
                v_z = xPred_z(1); 
            }
        } // pallav's modification
        if (KalmanPred) 
        {
            old_x = x;
            old_y = y;
            old_z = z;

            if(!v_x && !v_y && !v_z) 
            {
                KalmanPred = false;
            }
        }
        if (KalmanPred) 
        {
            Kalman_flag = true;
            Kalman_calc(x , v_x, y , v_y, z , v_z);
            circle(RGB, Point(min_x, min_y), 20, Scalar(0, 0, 255), 5);
        }

        if(countTimes>5)
        {
            posX += velX()*dt;
            posZ += velZ()*dt;
            if(abs(posX)>abs(xToMove) || abs(posZ)>abs(zToMove) && !ManualInput)
            {
                writeToArduino(0, 0);
            }
        }

        if (KalmanPred)
            predict(dt, RGB);
        else if (debug)
            writeToArduino(jp[0].x, jp[0].y);

        char ch =(char) waitKey(30);
        rc15_keyboard(ch);

        imshow("Kinect", RGB);

        // oldRgb = rgb.clone();
        // oldGray = gray.clone();
        // oldDepthMat = depthMat.clone(); // needed if the shuttle is tracked using filter based tracking
    }


    if(isFile)
    {
        writeToArduino(0,0);
    }

    device.stopVideo();
    device.stopDepth();
    destroyAllWindows();
    return 0;
}

