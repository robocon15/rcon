/*A program to test basic robot movements in a 2-D plane including 90 degree turns
*Written by Zaid Tasneem
*/

#include "twowheelbase.h"


Twowheelbase::Twowheelbase()
{
attachBase(-1,-1,-1,-1,-1,-1);
}

Twowheelbase::Twowheelbase(int Ldir1, int Ldir2 , int Lpwm, int Rdir1, int Rdir2, int Rpwm )
{
attachBase(Ldir1,Ldir2,Lpwm,Rdir1,Rdir2,Rpwm);
}

void Twowheelbase::attachBase(int Ldir1, int Ldir2 , int Lpwm, int Rdir1, int Rdir2, int Rpwm )
{
left_motor.attachMotor(Ldir1,Ldir2,Lpwm);
right_motor.attachMotor(Rdir1,Rdir2,Rpwm);
}

void Twowheelbase::setMeanSpeed(int Lmeanspeed, int Rmeanspeed)
{
left_motor.setMeanSpeed(Lmeanspeed);
right_motor.setMeanSpeed(Rmeanspeed);
}


//Movements




void Twowheelbase::moveForward(float Speed)
{
moveForward(Speed,Speed);
}

void Twowheelbase::moveForward(float Lspeed, float Rspeed)
{
left_motor.setMotorSpeed(Lspeed);
right_motor.setMotorSpeed(Rspeed);
}

void Twowheelbase::moveBackward(float Speed)
{
moveBackward(Speed,Speed);
}

void Twowheelbase::moveBackward(float Lspeed, float Rspeed)
{
moveForward(-Lspeed,-Rspeed);
}

// Rotate

void Twowheelbase::rotate(float Speed)
{
rotateRight(Speed);
 }
void Twowheelbase::rotateRight(float Speed)
{
moveForward(Speed,-Speed);
}
void Twowheelbase::rotateRight(float Lspeed, float Rspeed)
{
moveForward(Lspeed,-Rspeed);
}
void Twowheelbase::rotateLeft(float Speed)
{
moveForward(-Speed,Speed);
}
void Twowheelbase::rotateLeft(float Lspeed, float Rspeed)
{
moveForward(-Lspeed,Rspeed);
}

void Twowheelbase::stop()
{
left_motor.stopMotor();
right_motor.stopMotor();
}

void Twowheelbase::stopSmoothly()
{
	int i,j;
	for(i=abs(left_motor.speed),j=abs(right_motor.speed);i>=0;i=i-COUNT_CONST,j=j-abs(right_motor.speed)*COUNT_CONST/abs(left_motor.speed))
	{
		left_motor.changeSpeed(i);
		right_motor.changeSpeed(j);
		delay(COUNT_CONST*damping);
		left_motor.speed=i;
		right_motor.speed=j;
		left_motor.pwm=i*left_motor.mean_speed/100;
		right_motor.pwm=j*right_motor.mean_speed/100;
	}
	left_motor.changeSpeed(0);
	right_motor.changeSpeed(0);
	left_motor.speed=0;
	right_motor.speed=0;
	left_motor.pwm=0*left_motor.mean_speed/100;
	right_motor.pwm=0*right_motor.mean_speed/100;
}

void Twowheelbase::startSmoothly(float Speed)
{
	 startSmoothly(Speed,Speed);
}
void Twowheelbase::startSmoothly(float Lspeed,float Rspeed)
{

	float i,j;
	if(Lspeed>=0)
	{	
		digitalWrite(left_motor.dir1_pin,HIGH);
		digitalWrite(left_motor.dir2_pin,LOW);
	}
	if(Rspeed>=0)
	{	
		digitalWrite(right_motor.dir1_pin,HIGH);
		digitalWrite(right_motor.dir2_pin,LOW);
	}
	if(Lspeed<=0)
	{	
		digitalWrite(left_motor.dir1_pin,LOW);
		digitalWrite(left_motor.dir2_pin,HIGH);
	}
	if(Rspeed<=0)
	{	
		digitalWrite(right_motor.dir1_pin,LOW);
		digitalWrite(right_motor.dir2_pin,HIGH);
	}
	
	for(i=0,j=0;i<=abs(Lspeed);i=i+COUNT_CONST,j=j+abs(Rspeed)*COUNT_CONST/abs(Lspeed))
	{
		left_motor.changeSpeed(i);
		right_motor.changeSpeed(j);
		delay(COUNT_CONST*damping);
		left_motor.speed=i;
		right_motor.speed=j;
		left_motor.pwm=i*left_motor.mean_speed/100;
		right_motor.pwm=j*right_motor.mean_speed/100;
	}
		
 
}

void Twowheelbase::freeWheels()
{
left_motor.freeMotor();
right_motor.freeMotor();
}
/*void Twowheelbase::traverseCurve(float Radius, float Speed)
{
if(Radius>=0 && Speed >=0)
moveForward(Speed/Radius*(Radius +  WHEEL_DISTANCE/2), Speed/Radius*(Radius -  WHEEL_DISTANCE/2));
if(Radius>=0 && Speed <0)
moveBackward(Speed/Radius*(Radius -  WHEEL_DISTANCE/2), Speed/Radius*(Radius +  WHEEL_DISTANCE/2));
if(Radius<0 && Speed >=0)
moveForward(-Speed/Radius*(Radius -  WHEEL_DISTANCE/2),- Speed/Radius*(Radius +  WHEEL_DISTANCE/2));
if(Radius<0 && Speed <0)
moveBackward(-Speed/Radius*(Radius +  WHEEL_DISTANCE/2),- Speed/Radius*(Radius -  WHEEL_DISTANCE/2));
}
void Twowheelbase::coverDistanceSlowly(float Distance)
{
float i,j;
int a=V_MAX*V_MAX/Distance;
int time=2*sqrt(s/a);
digitalWrite(left_motor.dir1_pin,HIGH);
digitalWrite(left_motor.dir2_pin,LOW);
digitalWrite(right_motor.dir1_pin,HIGH);
digitalWrite(right_motor.dir2_pin,LOW);
for(i=0,j=0;i<=abs(V_MAX);i=i+COUNT_CONST,j=j+COUNT_CONST)
	{
		left_motor.changeSpeed(i);
		right_motor.changeSpeed(j);
		delay(COUNT_CONST/a);
		left_motor.speed=i;
		right_motor.speed=j;
		left_motor.pwm=i*left_motor.mean_speed/100;
		right_motor.pwm=j*right_motor.mean_speed/100;
	}
for(i=abs(V_MAX),j=abs(V_MAX);i>=0;i=i-COUNT_CONST,j=j-COUNT_CONST)
	{
		left_motor.changeSpeed(i);
		right_motor.changeSpeed(j);
		delay(COUNT_CONST/a);
		left_motor.speed=i;
		right_motor.speed=j;
		left_motor.pwm=i*left_motor.mean_speed/100;
		right_motor.pwm=j*right_motor.mean_speed/100;
	}
	left_motor.changeSpeed(0);
	right_motor.changeSpeed(0);
	left_motor.speed=0;
	right_motor.speed=0;
	left_motor.pwm=0*left_motor.mean_speed/100;
	right_motor.pwm=0*right_motor.mean_speed/100;
	
}*/