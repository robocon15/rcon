#include <Arduino.h>
#include <EncoderPID.h>
#include <PS2X_lib.h>
#include <Servo.h>
//#include <botconfig.h>

#ifndef FOUR_WHEEL_BASE_PS2_H
#define FOUR_WHEEL_BASE_PS2_H

#define CORDINATE_CONST 127

// Indices for buttons when sent via Laptop
#define PSL_R1 0
#define PSL_R2 1
#define PSL_L1 2
#define PSL_L2 3
#define PSL_BLUE 4
#define PSL_GREEN 5
#define PSL_RED 6

class Fourwheelbaseps2 : public Fourwheelbase , public PS2X , public Servo
{

//objects created by Rahul and Prince on 15-12-2013
public:
//variables
	int error; 												//variable for PS2
	byte type;												//variable for PS2
	byte vibrate;											//variable for PS2
	float speed_bot;
	float x;
	float y; 
	float a;
	float b;
	int analog_r_speed; 
	int analog_l_speed;
	int flag;
	int Pneu1;
	int Pneu2;
	int flagP;
	int Serve1;
	int Serve2;
	int ServePWM;
	int Servo;


public:
//Functions
Fourwheelbaseps2();
void attachFourBaseCurrent(int Dir_pinf1,int Dir_pinf2,int Pwm_pinf,int Dir_pinb1,int Dir_pinb2,int Pwm_pinb,int Dir_pinl1,int Dir_pinl2,int Pwm_pinl,int Dir_pinr1,int Dir_pinr2,int Pwm_pinr);
void attachPS2(uint8_t, uint8_t, uint8_t, uint8_t);
void attachPneumatic(int pneu1, int pneu2);
void attachService(int p1, int p2, int p3, int p4);
void setMeanSpeed(int Meanspeedf,int Meanspeedb,int Meanspeedl,int Meanspeedr);
int calc(int x_y);
void run();

//Laptop functions here
void run_laptop();
void InitialiseVars();
bool LButtonPressed(int index);
bool LButtonReleased(int index);
int LButtonArr[7];
int LButtonOld[7];
bool LButton(int index);
};

#endif
