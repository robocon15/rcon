#include <motor.h>
#include <newmotor.h>
#include <Servo.h>
#include <EncoderTest.h>
#ifndef SERVE
#define SERVE

class Service: public Motor, public Servo, public Encoder
{

	Encoder test;
	Motor m1;
	Servo myservo;

public:
	
	double angle1;
	int b;
	int frspeed;
	int initialAngle;
	int counter;

public:
	Service();
	Service(int p1, int p2, int p3, int p4, int p5, int p6);
	void attachServicePins(int p1, int p2, int p3, int p4, int p5, int p6);
	void setup1();
	void runSerialinput(int input);
	void serveRun();
	int getServiceAngle();
	void InterruptService();

};
#endif
