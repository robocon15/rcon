#ifndef _TRACKING_H_
#define _TRACKING_H_
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
void colorScore(Mat gray, vector<Point> *points, vector<int> *score);
void motionScore(Mat prevImg, Mat img, vector<Point> *pts, vector<int> *score);
void optFlowScore(Mat prevImg, Mat img, vector<Point> *points, vector<int> *score);
void cannyHoughScore(Mat gray, vector<Point> *points, vector<int> *score);
void clusterScore(vector<Point> *keypoints, vector<int> *score);
void depthScore(Mat depth, vector<Point> *pts, vector<int> *score);
void clean(vector<Point> *candidates, vector<int> *score, int min_score, Mat rgb, bool draw);

#endif
