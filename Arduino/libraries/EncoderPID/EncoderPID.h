#include <Arduino.h>
#include <twowheelbase.h>
#include <EncoderTest.h>
//#include <botconfig.h>

//Modified by Rahul Gupta and Prince Kumar

#ifndef FOUR_WHEEL_BASE_H
#define FOUR_WHEEL_BASE_H
#define encradius 0.02


class Fourwheelbase : public Twowheelbase, public Encoder
{
Twowheelbase pairx;
Twowheelbase pairy;

Encoder encx;
Encoder ency;

public:
//variables
float speed_rotate;
float curr_pos_x,curr_pos_y;
float final_pos_x,final_pos_y;
float theta;
int encoderPosX;
int encoderPosY;
int prev_posX;
int prev_posY;


 

//functions


Fourwheelbase();
Fourwheelbase(int Dir_pinf1,int Dir_pinf2,int Pwm_pinf,int Dir_pinb1,int Dir_pinb2,int Pwm_pinb,int Dir_pinl1,int Dir_pinl2,int Pwm_pinl,int Dir_pinr1,int Dir_pinr2,int Pwm_pinr);
void attachFourBase(int Dir_pinf1,int Dir_pinf2,int Pwm_pinf,int Dir_pinb1,int Dir_pinb2,int Pwm_pinb,int Dir_pinl1,int Dir_pinl2,int Pwm_pinl,int Dir_pinr1,int Dir_pinr2,int Pwm_pinr);
void meanSpeedCurrent(int Meanspeedf,int Meanspeedb,int Meanspeedl,int Meanspeedr);
void botForward(float Speed);
void botForward(float Speedl,float Speedr);									//to move forward move left and right motor and free forward and backward motor
void botBackward(float Speed);
void botBackward(float Speedl,float Speedr);									
void botLeft(float Speed);
void botLeft(float Speedf,float Speedb);
void botRight(float Speed);
void botRight(float Speedf,float Speedb);
void transAngle(float Theta,float Speed);
void transAngle(float Theta, float distance, float Speed);
void transAngleXY(float x, float y, float speed);
void transAnglePID(float Theta, float distance, float Speed);
void transSpeed(float Speedx,float Speedy);
//void rotateClockAngle(float Psi);
//void rotateAntiClockAngle(float Psi);
void rotateClockSpeed(float Speed);
void rotateAntiClockSpeed(float Speed);
void freeMotorCurrent(int Axisx,int Axisy);						//free/notfree left&right motors if Axisx 1/0,free/notfree forward&backward motors if Axisy 1/0   
void lockMotorCurrent(int Axisx,int Axisy);						//lock/notlock left&right motors if Axisx 1/0,lock/notlock forward&backward motors if Axisy 1/0 
void stopBot();
void traverseCurve(float Radius, float Speed);
void transAngleSmoothly(float Theta, float Distance);
void stopSmoothly(float A_x,float A_y, float Time);
void startSmoothly(float A_x,float A_y, float Time);

void CurrentPos();
void FinalPos();
};
#endif
