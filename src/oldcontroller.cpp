/*
 *	Controller commands library.
 *	Contains the characters sent to arduino for actions.
 *	Also contains the integer bindings of the PS3 controller.
 */

#include "../include/controller.h"
#include "../include/debug.h"
#include <unistd.h>

cJoystick::cJoystick() {
	active = false;
	joystick_fd = 0;
	joystick_ev = new js_event();
	joystick_st = new joystick_state();
	joystick_fd = open(JOYSTICK_DEV, O_RDONLY | O_NONBLOCK);
	if (joystick_fd > 0) {
		ioctl(joystick_fd, JSIOCGNAME(256), name);
		ioctl(joystick_fd, JSIOCGVERSION, &version);
		ioctl(joystick_fd, JSIOCGAXES, &axes);
		ioctl(joystick_fd, JSIOCGBUTTONS, &buttons);
		std::cout << "   Name: " << name << std::endl;
		std::cout << "Version: " << version << std::endl;
		std::cout << "   Axes: " << (int)axes << std::endl;
		std::cout << "Buttons: " << (int)buttons << std::endl;
		joystick_st->axis.reserve(axes);
		joystick_st->button.reserve(buttons);
		active = true;
		pthread_create(&thread, 0, &cJoystick::loop, this);
	}
}

cJoystick::~cJoystick() {
	if (joystick_fd > 0) {
		active = false;
		pthread_join(thread, 0);
		close(joystick_fd);
	}
	delete joystick_st;
	delete joystick_ev;
	joystick_fd = 0;
}

void* cJoystick::loop(void *obj) {
	while (reinterpret_cast<cJoystick *>(obj)->active) reinterpret_cast<cJoystick *>(obj)->readEv();
}

void cJoystick::readEv() {
	int bytes = read(joystick_fd, joystick_ev, sizeof(*joystick_ev));
	if (bytes > 0) {
		joystick_ev->type &= ~JS_EVENT_INIT;
		if (joystick_ev->type & JS_EVENT_BUTTON) {
			joystick_st->button[joystick_ev->number] = joystick_ev->value;
		}
		if (joystick_ev->type & JS_EVENT_AXIS) {
			joystick_st->axis[joystick_ev->number] = joystick_ev->value;
		}
	}
}

joystick_position cJoystick::joystickPosition(int n) {
	joystick_position pos;

	if (n > -1 && n < axes) {
		int i0 = n*2, i1 = n*2+1;
		float x0 = joystick_st->axis[i0]/32767.0f, y0 = -joystick_st->axis[i1]/32767.0f;
		float x  = x0 * sqrt(1 - pow(y0, 2)/2.0f), y  = y0 * sqrt(1 - pow(x0, 2)/2.0f);

		pos.x = x0;
		pos.y = y0;

		pos.theta = atan2(y, x);
		pos.r = sqrt(pow(y, 2) + pow(x, 2));
	} else {
		pos.theta = pos.r = pos.x = pos.y = 0.0f;
	}
	return pos;
}

bool cJoystick::buttonPressed(int n) {
	return n > -1 && n < buttons ? joystick_st->button[n] : 0;
}


int xInput = 0;			//	Whether controller is giving command in x axis at the moment.
int yInput = 0;			//	Whether controller is giving command in y axis at the moment.
bool Buttons[19];
char buttonsInByte = 0;
int buttons_to_be_sent[] = {KEY_r1, KEY_rt, KEY_l1, KEY_lt, KEY_x, KEY_t, KEY_o};

cJoystick js;			// The joystick struct
joystick_position jp[2];

void writeToArduino(double x, double y)
{
	buttonsInByte = 0;
	x = (1.0+x)*127;
	y = (1.0+y)*127;
	double a = (1.0+jp[1].x)*127.0;
	double b = (1.0+jp[1].y)*127.0;

	for(int i = 0; i<7; i++)
	{
		if(js.buttonPressed(i) && !Buttons[buttons_to_be_sent[i]])
		{
			buttonsInByte += pow(2,i);
		}
		if(i == KEY_lt)
		{
			debug = 0;
			if(!wasItDebug) {
				resetKalman();
				cout << "-------------------------\n";
			}
			wasItDebug = 1;
		}
		else
		{
			wasItDebug = 0;
			debug = 1;
		}
	}

	if(isFile)
	{
		FILE *file;
		file = fopen(ARD, "w");

		fprintf(file, "%c", 12);
		fprintf(file, "%c", (int)x);
		fprintf(file, "%c", (int)y);
		fprintf(file, "%c", (int)a);
		fprintf(file, "%c", (int)b);
		fprintf(file, "%c", buttonsInByte);

		fclose(file);
		usleep(20000);

		for(int iter_button=0; iter_button<19; iter_button++)
			if( js.buttonPressed(iter_button) )
				Buttons[iter_button] = 1;
			else
				Buttons[iter_button] = 0;
	}
}

void rc15_keyboard(char ch) {
	if(ch == 'r')
	{
		resetKalman();
		cv::waitKey(0);
	}

	else if(ch == 'p')
	{
		writeToArduino(0,0);
		while(cv::waitKey(0)!='p');
	}

	else if(ch == ' ')
	{
		std::cout<< "----------------------------------------\n";
	}
	else if (ch == 'x' || ch == 27) {
		writeToArduino(0,0);
		exit(0);
	}
	else if(ch == 't')
	{
		if(!debug){
			writeToArduino(0,0);
		}
		debug = !debug;
		std::cout << "-------------------------\n";
		if(!debug)
			resetKalman();
		cout << "Debug mode is " << debug << endl;
	}
}
