
#include <EncoderPID.h>
//0 degrees in direction of motor 1

Fourwheelbase::Fourwheelbase()
{
	attachFourBase(-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1);
}

Fourwheelbase::Fourwheelbase(int Dir_pinf1,int Dir_pinf2,int Pwm_pinf,int Dir_pinb1,int Dir_pinb2,int Pwm_pinb,int Dir_pinl1,int Dir_pinl2,int Pwm_pinl,int Dir_pinr1,int Dir_pinr2,int Pwm_pinr)
{
	attachFourBase(Dir_pinf1,Dir_pinf2,Pwm_pinf,Dir_pinb1,Dir_pinb2,Pwm_pinb,Dir_pinl1,Dir_pinl2,Pwm_pinl,Dir_pinr1,Dir_pinr2,Pwm_pinr);
	curr_pos_x = 0, curr_pos_y = 0;               
	final_pos_x = 0, final_pos_y = 0;
	encoderPosX = 0;
	encoderPosY = 0;
	prev_posX = 0;
	prev_posY = 0;
}



void Fourwheelbase::attachFourBase(int Dir_pinf1,int Dir_pinf2,int Pwm_pinf,int Dir_pinb1,int Dir_pinb2,int Pwm_pinb,int Dir_pinl1,int Dir_pinl2,int Pwm_pinl,int Dir_pinr1,int Dir_pinr2,int Pwm_pinr)
{
	 
	  pairx.attachBase(Dir_pinl1,Dir_pinl2,Pwm_pinl,Dir_pinr1,Dir_pinr2,Pwm_pinr);
	  pairy.attachBase(Dir_pinf1,Dir_pinf2,Pwm_pinf,Dir_pinb1,Dir_pinb2,Pwm_pinb);
	 
	  speed_rotate=50;
}

void Fourwheelbase::meanSpeedCurrent(int Meanspeedf,int Meanspeedb,int Meanspeedl,int Meanspeedr)
{
	pairy.setMeanSpeed(Meanspeedf,Meanspeedb);
	pairx.setMeanSpeed(Meanspeedl,Meanspeedr);
}

void Fourwheelbase::botForward(float Speed)				
{
	pairx.moveForward(Speed);
	pairy.stop();
}

void Fourwheelbase::botForward(float SpeedL,float SpeedR)
{
	pairx.moveForward(SpeedL,SpeedR);
	pairy.stop();
}

void Fourwheelbase::botBackward(float Speed)
{
	pairx.moveBackward(Speed);
	pairy.stop();
}

void Fourwheelbase::botBackward(float SpeedL,float SpeedR)
{
	pairx.moveBackward(SpeedL,SpeedR);
	pairy.stop();
}
void Fourwheelbase::botLeft(float Speed)
{
	pairy.moveBackward(Speed);
	pairx.stop();
}

void Fourwheelbase::botLeft(float SpeedF,float SpeedB)
{
	pairy.moveBackward(SpeedF,SpeedB);
	pairx.stop();
}

void Fourwheelbase::botRight(float Speed)
{
	pairy.moveForward(Speed);
	pairx.stop();
}

void Fourwheelbase::botRight(float SpeedF,float SpeedB)
{
	pairy.moveForward(SpeedF,SpeedB);
	pairx.stop();
}

void Fourwheelbase::transAngle(float Theta,float Speed)
{	
	pairx.moveForward(Speed*cos(3.1415*Theta/180));
	pairy.moveForward(Speed*sin(3.1415*Theta/180));
}

void Fourwheelbase::transAngleXY(float x, float y, float Speed)
{
	float r = sqrt(x*x + y*y);
	if (x > 0)
	{
		if (y > 0)
			theta = atan(y / x);
		else
			theta = -atan(y / x);
	}
	else
	{
		if (y > 0)
			theta = 180 - 180 / 3.1415*atan(-y / x);
		else
			theta = 180 + 180 / 3.1415*atan(-y / -x);
	}
	transAngle(theta, r, Speed);
}

void Fourwheelbase::transAngle(float Theta, float distance, float Speed)
{	
	theta = Theta;
	final_pos_x = distance*cos(3.1415*theta / 180);
	final_pos_y = distance*sin(3.1415*theta / 180);
	while (abs(final_pos_x - curr_pos_x) >= 0.02 && abs(final_pos_y - curr_pos_y) >= 0.02)
	{
		//minimizing err
		prev_posX += encoderPosX;                                       //updating prev enc position 
		prev_posY += encoderPosY;                                           
		encoderPosX = encx.getEncoderPos();                            //getting absolute encoder position 
		encoderPosY = ency.getEncoderPos();                            //getting absolute encoder position 
		encoderPosX = encx.getEncoderPos() - prev_posX;                    //diffence of previous encpositon and current encposition
		encoderPosY = ency.getEncoderPos() - prev_posY;                    //diffence of previous encpositon and current encposition
		pairx.moveForward(Speed*cos(3.1415*theta / 180));
		pairy.moveForward(Speed*sin(3.1415*theta / 180));
		CurrentPos();
		FinalPos();

		theta = 180/3.1415*atan(abs(final_pos_y / final_pos_x));

		if (final_pos_y > 0)
		{
			if (final_pos_x > 0)
				theta = theta;
			else
				theta = 180-theta;
		}
		else
		{
			if (final_pos_x > 0)
				
				theta = 360 - theta;
			else
				theta = 180 + theta;
		}
		
	}
	stopBot();
}

void Fourwheelbase::transAnglePID(float Theta, float distance, float Speed)
{	
	
	final_pos_x = distance*cos(3.1415*Theta / 180);
	final_pos_y = distance*sin(3.1415*Theta / 180);
	while (abs(final_pos_x - curr_pos_x) >= 0.02 && abs(final_pos_y - curr_pos_y) >= 0.02)
	{
	//minimizing err
	prev_posX += encoderPosX;                                       //updating prev enc position 
	prev_posY += encoderPosY;                                           
	encoderPosX = encx.getEncoderPos();                            //getting absolute encoder position 
	encoderPosY = ency.getEncoderPos();                            //getting absolute encoder position 
	encoderPosX = encx.getEncoderPos() - prev_posX;                //diffence of previous encpositon and current encposition
	encoderPosY = ency.getEncoderPos() - prev_posY;                //diffence of previous encpositon and current encposition
	
	pairx.moveForward(Speed*cos(3.1415*theta / 180));
	pairy.moveForward(Speed*sin(3.1415*theta / 180));
	CurrentPos();
	if (final_pos_x / final_pos_y*curr_pos_y - curr_pos_x > 0)
		botRight(Speed);
	else
		botLeft(Speed);
	}
    stopBot();

}


void Fourwheelbase::transSpeed(float Speedx,float Speedy)
{
	pairx.moveForward(Speedx);
	pairy.moveForward(Speedy);
}

void Fourwheelbase::rotateClockSpeed(float Speed)
{
	 pairx.rotate(Speed);
	 pairy.rotate(Speed);
}

void Fourwheelbase::rotateAntiClockSpeed(float Speed)
{
	pairx.rotate(-Speed);
	pairy.rotate(-Speed);
}

void Fourwheelbase::freeMotorCurrent(int Axisx,int Axisy)
{
	if(Axisx==1)
		pairx.freeWheels();
	if(Axisy==1)
		pairy.freeWheels();
}

void Fourwheelbase::lockMotorCurrent(int Axisx,int Axisy)					
{
	if(Axisx==1)
		pairx.stop();
	if(Axisy==1)
		pairy.stop();
}
void Fourwheelbase::stopBot()
{
	pairx.stop();
	pairy.stop();
}

void Fourwheelbase::CurrentPos()                                  //for determining current position of bot       
{
    curr_pos_x= (2*3.1415*encradius*encoderPosX) / PPR;             
    curr_pos_y= (2*3.1415*encradius*encoderPosY) / PPR;
}

void Fourwheelbase::FinalPos()                                    //for updating final position w.r.t bot
{
	final_pos_x-=curr_pos_x;
    final_pos_y-=curr_pos_y;
	curr_pos_x = 0;
	curr_pos_y = 0;
}




/*void Fourwheelbase::traverseCurve(float Radius, float Speed)
{
pairx.moveForward(Speed/Radius*(Radius +  WHEEL_DISTANCE/2), Speed/Radius*(Radius -  WHEEL_DISTANCE/2));
pairy.rotateRight(Speed/Radius*WHEEL_DISTANCE/2);
}

void Fourwheelbase::startSmoothly(float A_x,float A_y, float Time)
{
float i,j;
for(i=0,j=0;i<=abs(A_x*Time);i=i+COUNT_CONST,j=j+COUNT_CONST*A_x/A_y)
	{   pairx.moveForward(i);
		pairy.moveForward(j);
		delay(COUNT_CONST/A_x);	
	}
}

void Fourwheelbase::stopSmoothly(float A_x,float A_y, float Time)
{
float i,j;
for(i=abs(A_x*Time),j=abs(A_y*Time);i>=0;i=i-COUNT_CONST,j=j-COUNT_CONST*A_y/A_x)
	{   pairx.moveForward(i);
		pairy.moveForward(j);
		delay(COUNT_CONST/A_x);	
	}
	pairx.stop();
	pairy.stop();
}

void Fourwheelbase::transAngleSmoothly(float Theta, float Distance) // changed by Apagon Silvestres (Amartya) (Jungli Andhkaar)
{
float R=Distance; //changed by mukund 
int x=R*cos(Theta);
int y=R*sin(Theta);
int time_acc=0;
int time_const=0;int a_x=0;
int a_y=0;
if(Theta<=45)
{
if(V_MAX*V_MAX/A_MAX<x)
{
 time_acc=V_MAX/A_MAX;
 time_const=(x-V_MAX*V_MAX/A_MAX)/V_MAX;           //v_y=time_acc*a_y
 a_x=A_MAX;
a_y=y/(time_acc*time_acc + time_acc*time_const);	  //y=v_y*v_y/a_y+v_y*time_const
startSmoothly(a_x,a_y,time_acc);
pairx.moveForward(V_MAX);
pairy.moveForward(time_acc*a_y);
delay(1000*time_const);
stopSmoothly(a_x,a_y,time_acc);
}
else
{
time_acc=2*sqrt(x/A_MAX);
a_x=A_MAX;
a_y=4*y/(time_acc*time_acc);
startSmoothly(a_x,a_y,time_acc);
stopSmoothly(a_x,a_y,time_acc);
}
}
else
{
if(V_MAX*V_MAX/A_MAX<y)
{
 time_acc=V_MAX/A_MAX;
 time_const=(y-V_MAX*V_MAX/A_MAX)/V_MAX;           //v_y=time_acc*a_y
 a_y=A_MAX;
a_x=x/(time_acc*time_acc + time_acc*time_const);	  //y=v_y*v_y/a_y+v_y*time_const
startSmoothly(a_x,a_y,time_acc);
pairx.moveForward(time_acc*a_x);
pairy.moveForward(time_acc*a_y);
delay(1000*time_const);
stopSmoothly(a_x,a_y,time_acc);
}
else
{
time_acc=2*sqrt(y/A_MAX);
a_y=A_MAX;
a_x=4*x/(time_acc*time_acc);
startSmoothly(a_x,a_y,time_acc);
stopSmoothly(a_x,a_y,time_acc);
}

}*/
