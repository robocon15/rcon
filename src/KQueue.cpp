#include <bits/stdc++.h>
#include <armadillo>
#include <opencv2/opencv.hpp>
#define QSize 5
#define FIRST_DATA 1
#define TOO_FAR -1
#define DUPLICATE_DATA -2
#define NO_PREDICTION -3

using namespace std;
using namespace cv;

typedef struct COORD {
	double x;
	double y;
	double z;
	double dt;
} coord;

class Kalman
{
	public:
		coord CQ[QSize];
		int size_q ;
		int curr_q ;
		Kalman();
		void enqueue(coord pts);
	
		double get_dt();

		int sendData(double x, double y, double z);		
		bool duplicate(double x, double y, double z);
		int sendToKalman(double x, double y, double z, double dt);
		int sendToKalman();
		int predict();
		bool   success;
		int    turn;
		bool   normal;
		double x_prev, y_prev, z_prev;

		float k   ;
		float m   ;
		float g_x ;
		float g_y ;
		float g_z ;
		float dt  ;

		bool Kalman_flag;

		arma::mat A_x;
		arma::mat P_x;
		arma::mat Q_x;
		arma::mat B_x;
		arma::mat H_x;
		arma::mat R_x;
		arma::mat X_x;
		arma::mat U_x;

		arma::mat A_y;
		arma::mat P_y;
		arma::mat Q_y;
		arma::mat B_y;
		arma::mat H_y;
		arma::mat R_y;
		arma::mat X_y;
		arma::mat U_y;

		arma::mat A_z;
		arma::mat P_z;
		arma::mat Q_z;
		arma::mat B_z;
		arma::mat H_z;
		arma::mat R_z;
		arma::mat X_z;
		arma::mat U_z;

		arma::mat Pred_x; 
		arma::mat Pred_y; 
		arma::mat Pred_z; 
};

Kalman::Kalman()
{
	success = false;
	Kalman_flag = 0;
	turn=0;
	normal = false;
	x_prev=0;
   	y_prev=0;
   	z_prev=0;
	size_q = 0;
	curr_q = 0;
	k   = 0.003;
	m   = 0.005;		//	SI UNITS
	g_x = 0;
	g_y = 9.80665;
	g_z = 0;
	dt  = 0.04;
	Pred_x = arma::mat(2, 1, arma::fill::zeros);
	Pred_y = arma::mat(2, 1, arma::fill::zeros);
    Pred_z = arma::mat(2, 1, arma::fill::zeros);
}
void Kalman::enqueue(coord pts) {
	CQ[curr_q] = pts;
	curr_q = (curr_q + 1) % QSize;
	size_q += (size_q < QSize ? 1 : 0);
}
bool Kalman::duplicate(double x, double y, double z) {
	double dist = (x-x_prev)*(x-x_prev) + (y-y_prev)*(y-y_prev) + (z-z_prev)*(z_prev);
	if(dist<0.000001)
		return true;
	return false;
}
double Kalman::get_dt() {
	// Returns the time between two frames
	double t2 = cv::getTickCount();
	double freq = cv::getTickFrequency();
	static double t1 = t2 - freq*0.04;
	double dt = ((t2-t1)/freq);
	t1 = t2;
	return dt;
}

int Kalman::sendToKalman() {
	success = false;
	A_x = arma::mat(2,2,arma::fill::eye);
	P_x = arma::mat(2,2,arma::fill::eye);
	Q_x = arma::mat(2,2,arma::fill::eye);
	B_x = arma::mat(2,2,arma::fill::eye);
	H_x = arma::mat(2,2,arma::fill::eye);
	R_x = arma::mat(2,2,arma::fill::eye);
	X_x = arma::mat(2,1,arma::fill::zeros);
	U_x = arma::mat(2,1,arma::fill::zeros);

	A_y = arma::mat(2,2,arma::fill::eye);
	P_y = arma::mat(2,2,arma::fill::eye);
	Q_y = arma::mat(2,2,arma::fill::eye);
	B_y = arma::mat(2,2,arma::fill::eye);
	H_y = arma::mat(2,2,arma::fill::eye);
	R_y = arma::mat(2,2,arma::fill::eye);
	X_y = arma::mat(2,1,arma::fill::zeros);
	U_y = arma::mat(2,1,arma::fill::zeros); 

	A_z = arma::mat(2,2,arma::fill::eye);
	P_z = arma::mat(2,2,arma::fill::eye);
	Q_z = arma::mat(2,2,arma::fill::eye);
	B_z = arma::mat(2,2,arma::fill::eye);
	H_z = arma::mat(2,2,arma::fill::eye);
	R_z = arma::mat(2,2,arma::fill::eye);
	X_z = arma::mat(2,1,arma::fill::zeros);
	U_z = arma::mat(2,1,arma::fill::zeros); 
	for(int i=curr_q; i<(curr_q+size_q)%QSize; i++)
	{
		sendToKalman(CQ[i].x, CQ[i].y, CQ[i].z, CQ[i].dt);
	}
	return 0;
}

int Kalman::predict() {
	double dt = 0.01;
	int t = 300;
	arma::mat Prediction_x = A_x*X_x + B_x*U_x;
	arma::mat Prediction_y = A_y*X_y + B_y*U_y;
	arma::mat Prediction_z = A_z*X_z + B_z*U_z;
	while(t--)
	{
		if(Prediction_y(0, 0) < 0.05 && Prediction_y(1, 0) < 0)
		{
			Pred_x(0, 0) = Prediction_x(0,0);
			Pred_x(1, 0) = Prediction_x(1,0);
			Pred_y(0, 0) = Prediction_y(0,0);
			Pred_y(1, 0) = Prediction_y(1,0);
			Pred_z(0, 0) = Prediction_z(0,0);
			Pred_z(1, 0) = Prediction_z(1,0);
			if(size_q >=5 )
			{
				double z = abs(Pred_z(0, 0));
				double x = abs(Pred_x(0, 0));
				if(z<1 && x<1)
					normal = true;
			}
			return 0;
		}
		Prediction_x = A_x*Prediction_x + B_x*U_x;
		Prediction_y = A_y*Prediction_y + B_y*U_y;
		Prediction_z = A_z*Prediction_z + B_z*U_z;
	}
	return -1;
}

int Kalman::sendToKalman(double x, double y, double z, double dt) {
	double v_x = (x-x_prev)/dt;
	double v_y = (y-y_prev)/dt;
	double v_z = (z-z_prev)/dt;


	U_y 	<<	-0.5*g_y*dt*dt 		<< 	arma::endr
			<<	    -g_y*dt 		<<	arma::endr;
	A_x		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
	A_y		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
	A_z		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
	arma::mat xPred_x = A_x * X_x + B_x * U_x;
	arma::mat xPred_y = A_y * X_y + B_y * U_y;
	arma::mat xPred_z = A_z * X_z + B_z * U_z;


	x 	= xPred_x(0);
	v_x = xPred_x(1);
	y 	= xPred_y(0);
	v_y = xPred_y(1);
	z 	= xPred_z(0);
	v_z = xPred_z(1);
	arma::mat Z_x;
	//arma::mat xPred_x;
	arma::mat pPred_x;
	arma::mat y_x;
	arma::mat S_x;
	arma::mat K_x;
	arma::mat I_x = arma::mat(2,2,arma::fill::eye);

	Z_x << x << arma::endr << v_x << arma::endr;

	xPred_x   = A_x*X_x + B_x*U_x;
	pPred_x   = A_x*P_x*arma::trans(A_x) + Q_x;
	y_x 	  = Z_x - H_x*xPred_x;
	S_x 	  = H_x*pPred_x*arma::trans(H_x) + R_x;
	K_x 	  = pPred_x*arma::trans(H_x)*arma::inv(S_x);
	X_x 	  = xPred_x + K_x*y_x;
	P_x 	  = (I_x - K_x*H_x)*pPred_x;


	arma::mat Z_y;
	//arma::mat xPred_y;
	arma::mat pPred_y;
	arma::mat y_y;
	arma::mat S_y;
	arma::mat K_y;
	arma::mat I_y = arma::mat(2,2,arma::fill::eye);

	Z_y << y << arma::endr << v_y << arma::endr;

	xPred_y   = A_y*X_y + B_y*U_y;
	pPred_y   = A_y*P_y*arma::trans(A_y) + Q_y;
	y_y 	  = Z_y - H_y*xPred_y;
	S_y 	  = H_y*pPred_y*arma::trans(H_y) + R_y;
	K_y 	  = pPred_y*arma::trans(H_y)*arma::inv(S_y);
	X_y 	  = xPred_y + K_y*y_y;
	P_y 	  = (I_y - K_y*H_y)*pPred_y;


	arma::mat Z_z;
	//arma::mat xPred_z;
	arma::mat pPred_z;
	arma::mat y_z;
	arma::mat S_z;
	arma::mat K_z;
	arma::mat I_z = arma::mat(2,2,arma::fill::eye);

	Z_z << z << arma::endr << v_z << arma::endr;

	xPred_z   = A_z*X_z + B_z*U_z;
	pPred_z   = A_z*P_z*arma::trans(A_z) + Q_z;
	y_z 	  = Z_z - H_z*xPred_z;
	S_z 	  = H_z*pPred_z*arma::trans(H_z) + R_z;
	K_z 	  = pPred_z*arma::trans(H_z)*arma::inv(S_z);
	X_z 	  = xPred_z + K_z*y_z;
	P_z 	  = (I_z - K_z*H_z)*pPred_z;
	return 0;	
}

int Kalman::sendData(double x, double y, double z) {
	if(z>8)
		return TOO_FAR;

	if(duplicate(x,y,z))
		return DUPLICATE_DATA;

	turn++;

	if(turn == 1)
	{
		enqueue( (coord){x, y, z, get_dt()} );
		return FIRST_DATA;
	}

	if(normal)
	{
		sendToKalman(x, y, z, get_dt());
		int dbg = predict();
		if(!dbg)
			return NO_PREDICTION;
	}
	else
	{
		enqueue((coord){x, y, z, get_dt()});
		sendToKalman();
		int dbg = predict();
		if(!dbg)
			return NO_PREDICTION;
	}
	
	x_prev = x;
	y_prev = y;
	z_prev = z;
}
