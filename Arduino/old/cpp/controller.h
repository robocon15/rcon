/*
 *	Controller commands library.
 *	Contains the characters sent to arduino for actions.
 *	Also contains the integer bindings of the PS3 controller.
 */

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <iostream>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>
#include <linux/joystick.h>
#include <vector>
#include <unistd.h>

#include "kalman.h"
#include "debug.h"

#define JOYSTICK_DEV "/dev/input/js0"

#define PSL_R1 0
#define PSL_R2 1
#define PSL_L1 2
#define PSL_L2 3
#define PSL_BLUE 4
#define PSL_GREEN 5
#define PSL_RED 6


struct joystick_position {
  float theta, r, x, y;
};

struct joystick_state {
  std::vector<signed short> button;
  std::vector<signed short> axis;
};

class cJoystick {
 private:
  pthread_t thread;
  bool active;
  int joystick_fd;
  js_event *joystick_ev;
  joystick_state *joystick_st;
  __u32 version;
  __u8 axes;
  __u8 buttons;
  char name[256];

 protected:
 public:
  cJoystick();
  ~cJoystick();
  static void* loop(void* obj);
  void readEv();
  joystick_position joystickPosition(int n);
  bool buttonPressed(int n);
};


extern int xInput;             // Whether controller is giving command in x axis at the moment.
extern int yInput;             // Whether controller is giving command in y axis at the moment.
extern bool Buttons[19];

extern cJoystick js;
extern joystick_position jp[2];

void processInput();
void getJoyMotion();
void ArdJoyMotion();

void rc15_keyboard(char ch);

#endif
