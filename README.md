# README #

## Overview

This repository contains the code for tracking and predicting the path of a moving shuttlecock.

## Dependencies

* OpenCV 2.4.8
* libfreenect
* libusb

## Getting Started

To generate executables run `make`

## Error messages

```
$ make
g++ -c src/util.cpp -o bin/util.o
Assembler messages:
Fatal error: can't create bin/util.o: No such file or directory
make: *** [bin/util.o] Error 1
```
__Solution:__ create the `bin` directory in the repository by running `mkdir bin`